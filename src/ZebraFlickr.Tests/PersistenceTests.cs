﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Tests
{
    [TestFixture]
    public class PersistenceTests
    {
        #region Setup/Teardown

        [SetUp]
        public void SetUp()
        {
            _store = new PersistentStore(":memory:");
        }

        [TearDown]
        public void TearDown()
        {
            if (_store != null)
                _store.Dispose();
        }

        #endregion

        private PersistentStore _store;

        private static void CompareFileEntries(FileEntry file, FileEntry dbFile)
        {
            Assert.AreEqual(file.Name, dbFile.Name);
            Assert.AreEqual(file.ContentChanged, dbFile.ContentChanged);
            Assert.AreEqual(file.Modified, dbFile.Modified);
            Assert.AreEqual(file.Uploaded, dbFile.Uploaded);
            Assert.AreEqual(file.Size, dbFile.Size);
            Assert.AreEqual(file.UploadedMetaData, dbFile.UploadedMetaData);
            Assert.AreEqual(file.PictureId, dbFile.PictureId);
            Assert.AreEqual(file.MetaDataChanged, dbFile.MetaDataChanged);
            Assert.AreEqual(file.Skip, dbFile.Skip);
            Assert.AreEqual(file.UploadedSet, dbFile.UploadedSet);
            Assert.AreEqual(file.Hash, dbFile.Hash);
            Assert.AreEqual(file.PicasaValues.Count, dbFile.PicasaValues.Count);
        }

        [Test]
        public void Can_add_file()
        {
            var file = new FileEntry
                {
                    Name = "testing 123",
                    ContentChanged = true,
                    Modified = DateTime.Now,
                    Uploaded = true,
                    Size = 1203,
                    UploadedMetaData = true,
                    PictureId = "234",
                    MetaDataChanged = true,
                    Skip = true,
                    UploadedSet = true,
                    Hash = new byte[] {1, 2, 3},
                    PicasaValues = new Dictionary<string, string>
                        {
                            {"SDF", "sdfsdf"},
                            {"SDF2", "sdfsdf"},
                        },
                };

            using (var work = _store.Begin())
            {
                work.Add(file);

                var dbFile = work.Get<FileEntry>().First();

                Assert.IsNotNull(dbFile);
                CompareFileEntries(file, dbFile);
            }
        }

        [Test]
        public void Can_create_set()
        {
            using (var work = _store.Begin())
            {
                work.Add(new SetEntry {Name = "test", SetId = "1234"});

                var set = work.Get<SetEntry>().First();

                Assert.IsNotNull(set);
                Assert.AreEqual("test", set.Name);
                Assert.AreEqual("1234", set.SetId);
            }
        }

        [Test]
        public void Can_query_set()
        {
            using (var work = _store.Begin())
            {
                work.Add(new SetEntry { Name = "test", SetId = "1234" });

                var set = work.FirstOrDefault<SetEntry>(x => x.Name == "test");

                Assert.IsNotNull(set);
                Assert.AreEqual("test", set.Name);
                Assert.AreEqual("1234", set.SetId);
            }
        }

        [Test]
        public void Can_store_user_details()
        {
            var details = new UserDetails
                {
                    FullName = "a",
                    TokenSecret = "b",
                    UserId = "c",
                    Token = "d",
                    Username = "e"
                };

            using (var work = _store.Begin())
            {
                work.Add(details);

                var dbDetails = work.FirstOrDefault<UserDetails>();

                Assert.IsNotNull(dbDetails);
                Assert.AreEqual(details.FullName, dbDetails.FullName);
                Assert.AreEqual(details.TokenSecret, dbDetails.TokenSecret);
                Assert.AreEqual(details.UserId, dbDetails.UserId);
                Assert.AreEqual(details.Token, dbDetails.Token);
                Assert.AreEqual(details.Username, dbDetails.Username);
            }
        }

        [Test]
        public void Can_update_file()
        {
            Can_add_file();
            
            using (var work = _store.Begin())
            {
                var dbFile = work.Get<FileEntry>().First();

                dbFile.Modified = DateTime.Now;
                dbFile.PicasaValues.Add("1", "1");
                dbFile.Uploaded = false;
                dbFile.Skip = false;
                dbFile.ContentChanged = false;

                work.Update(dbFile);

                var dbFile2 = work.Get<FileEntry>().First();

                Assert.IsNotNull(dbFile2);
            }
        }

        [Test]
        public void Doesnt_crash_for_no_user_details()
        {
            using (var work = _store.Begin())
            {
                var details = work.FirstOrDefault<UserDetails>();

                Assert.IsNull(details);
            }
        }
    }
}