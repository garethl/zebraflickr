﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ZebraFlickr.Core
{
    public class MemoryCache<TKey, TValue>
    {
        private readonly ConcurrentDictionary<TKey, TValue> _cache;

        public MemoryCache()
            : this(EqualityComparer<TKey>.Default)
        {
        }

        public MemoryCache(IEqualityComparer<TKey> comparer)
        {
            _cache = new ConcurrentDictionary<TKey, TValue>(comparer);
        }

        public TValue this[TKey key]
        {
            get
            {
                TValue ret;
                _cache.TryGetValue(key, out ret);
                return ret;
            }
            set { _cache[key] = value; }
        }

        public IEnumerable<TKey> Keys
        {
            get { return _cache.Keys; }
        } 

        public IEnumerable<TValue> Values
        {
            get { return _cache.Values; }
        }

        public int Count
        {
            get { return _cache.Count; }
        }

        public void Fill(IEnumerable<TValue> values, Func<TValue, TKey> getKey)
        {
            foreach (var value in values)
            {
                _cache[getKey(value)] = value;
            }
        }

        public bool Remove(TKey key)
        {
            TValue value;
            return _cache.TryRemove(key, out value);
        }

        public bool Remove(TKey key, out TValue value)
        {
            return _cache.TryRemove(key, out value);
        }
    }
}