﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using IniParser;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Core
{
    public class MetaDataHelper
    {
        private readonly Dictionary<string, Dictionary<string, KeyDataCollection>> _picasaValues =
            new Dictionary<string, Dictionary<string, KeyDataCollection>>(StringComparer.OrdinalIgnoreCase);

        public bool RefreshMetaData(FileInfo file, FileEntry entry)
        {
            //check that the meta data hasn't changed
            var picasaValues = GetPicasaValues(file.FullName);

            if (picasaValues.ContainsKey(file.Name))
            {
                var values = picasaValues[file.Name];

                var changed = IsPicasaValuesChanged(entry.PicasaValues, values);

                if (changed)
                {
                    if (entry.PicasaValues == null)
                        entry.PicasaValues = new Dictionary<string, string>();
                    else
                        entry.PicasaValues.Clear();

                    foreach (var value in values)
                        entry.PicasaValues[value.KeyName] = value.Value;

                    if (entry.Id > 0)
                        entry.MetaDataChanged = true;

                    return true;
                }
            }

            return false;
        }

        public static bool IsPicasaValuesChanged(Dictionary<string, string> existing, KeyDataCollection values)
        {
            bool changed = false;

            if ((existing != null && existing.Count != 0) || values.Count != 0)
            {
                if (existing == null)
                    return true;

                if (values.Count != existing.Count)
                {
                    changed = true;
                }
                else
                {
                    foreach (var value in values)
                    {
                        string stored;
                        if (existing.TryGetValue(value.KeyName, out stored))
                        {
                            if (stored != value.Value)
                            {
                                changed = true;
                                break;
                            }
                        }
                        else
                        {
                            changed = true;
                            break;
                        }
                    }
                }
            }

            return changed;
        }

        public Dictionary<string, KeyDataCollection> GetPicasaValues(string fullName)
        {
            var directory = Path.GetDirectoryName(fullName);

            Dictionary<string, KeyDataCollection> ret;
            if (!_picasaValues.TryGetValue(directory, out ret))
            {
                var picasaFile = Directory.GetFiles(directory, "*picasa.ini");

                ret = new Dictionary<string, KeyDataCollection>(StringComparer.OrdinalIgnoreCase);
                if (picasaFile.Length != 0)
                {
                    var parser = new FileIniDataParser();
                    var iniData = parser.LoadFile(picasaFile[0]);

                    foreach (var section in iniData.Sections)
                    {
                        ret[section.SectionName] = section.Keys;
                    }
                }

                _picasaValues[directory] = ret;
            }

            return ret;
        }
    }
}