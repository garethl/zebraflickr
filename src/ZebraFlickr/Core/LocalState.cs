﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Core
{
    public class LocalState : IDisposable
    {
        private readonly LocalStateCache _cache;
        private readonly Config _config;

        private readonly MetaDataHelper _metaDataHelper = new MetaDataHelper();
        private readonly PersistentStore _store;

        public LocalState(Config config)
        {
            TempStore.Initialize(config);

            _config = config;

            _store = new PersistentStore(_config.HistoryFile);

            Log.Debug("Loading history file {0}", _config.HistoryFile);

            _cache = new LocalStateCache(_store);
        }

        public Config Config
        {
            get { return _config; }
        }

        public IEnumerable<FileEntry> SkippedFiles
        {
            get
            {
                foreach (var entry in _cache.FilesByName.Values.Where(entry => entry.Skip).OrderBy(x => x.Name))
                    yield return entry;
            }
        }

        public StoreCache Cache
        {
            get { return _store.Cache; }
        }

        public LocalStateCache Lookup
        {
            get { return _cache; }
        }

        public FileEntry GetEntry(string root, FileInfo file)
        {
            var name = GetName(root, file.FullName);

            var entry = _cache.FilesByName[name];
            if (entry != null)
            {
                using (var work = _store.Begin())
                {
                    //if the size and modified date are exactly the same, we assume the contents are the same
                    if (entry.Size != file.Length || (!(Math.Abs((entry.Modified - file.LastWriteTimeUtc).TotalSeconds) <= 1)))
                    {
                        var hash = HashCalculator.CalculateHash(file.FullName);

                        if (ByteArrayComparer.Instance.Equals(entry.Hash, hash))
                        {
                            //hash is the same, update the record
                            entry.Size = file.Length;
                            entry.Modified = file.LastWriteTimeUtc;
                            work.Update(entry);
                        }
                        else
                        {
                            //check to see if the file hash matches another file hash... (i.e. file has moved)
                            FileEntry existingEntry;
                            if (HasEntryMoved(root, hash, out existingEntry))
                            {
                                RemoveExistingEntry(work, existingEntry, entry);
                            }
                            else
                            {
                                //the file has changed
                                entry.Hash = hash;
                                entry.ContentChanged = true;
                                entry.MetaDataChanged = true;
                                entry.ExifHash = null;
                                work.Update(entry);
                            }
                        }
                    }

                    //check that the meta data hasn't changed
                    if (_metaDataHelper.RefreshMetaData(file, entry))
                        work.Update(entry);
                }

                return entry;
            }

            entry = new FileEntry
                {
                    Name = name,
                    Modified = file.LastWriteTimeUtc,
                    Hash = HashCalculator.CalculateHash(file.FullName),
                    Size = file.Length,
                };

            // check to see if the file hash matches another file hash... (i.e. file has moved)
            {
                FileEntry existingEntry;
                if (HasEntryMoved(root, entry.Hash, out existingEntry))
                {
                    using (var work = _store.Begin())
                    {
                        RemoveExistingEntry(work, existingEntry, entry);
                    }
                }
                else if (existingEntry != null)
                {
                    //the existing one has been uploaded, link it to that one
                    entry.PictureId = existingEntry.PictureId;
                    entry.Uploaded = existingEntry.Uploaded;
                    entry.UploadedMetaData = existingEntry.UploadedMetaData;
                    entry.UploadedSet = false;
                    entry.MetaDataChanged = true;
                }
            }

            _metaDataHelper.RefreshMetaData(file, entry);

            AddEntry(entry);
            return entry;
        }

        private void RemoveExistingEntry(StoreWork work, FileEntry entryToDelete, FileEntry replacementEntry)
        {
            //delete the entry
            work.Delete<FileEntry>(entryToDelete.Id);
            Lookup.Deleted(entryToDelete);

            if (replacementEntry == null)
                return;

            Lookup.FilesByHash[replacementEntry.Hash] = replacementEntry;

            if (entryToDelete.PictureId == replacementEntry.PictureId)
            {
                //ensure we set the cache correctly
                Lookup.FilesByPhotoId[replacementEntry.PictureId] = replacementEntry;
            }
        }

        private bool HasEntryMoved(string root, byte[] hash, out FileEntry existingEntry)
        {
            existingEntry = Lookup.FilesByHash[hash];

            if (existingEntry == null)
                return false;

            return !File.Exists(Path.Combine(root, existingEntry.Name));
        }

        public FileEntry GetEntryByHash(string hashValue)
        {
            var hash = hashValue.FromHex();

            return GetEntryByHash(hash);
        }

        public FileEntry GetEntryByHash(byte[] hash)
        {
            return _cache.FilesByHash[hash];
        }

        private string GetName(string root, string fullName)
        {
            return fullName.Substring(root.Length + 1);
        }

        public UserDetails GetUserDetails()
        {
            using (var work = _store.Begin())
            {
                return work.FirstOrDefault<UserDetails>();
            }
        }

        public void StoreUserDetails(UserDetails details)
        {
            using (var work = _store.Begin())
            using (var trans = work.BeginTransaction())
            {
                work.DeleteAll<UserDetails>();
                work.Add(details);

                trans.Commit();
            }
        }

        public void UpdateEntry(FileEntry entry)
        {
            using (var work = _store.Begin())
                work.Update(entry);
        }

        public void AddEntry(FileEntry entry)
        {
            using (var work = _store.Begin())
                work.Add(entry);

            _cache.FilesByName[entry.Name] = entry;
            _cache.FilesByHash[entry.Hash] = entry;
            if (!string.IsNullOrEmpty(entry.PictureId))
                _cache.FilesByPhotoId[entry.PictureId] = entry;
        }

        public SetEntry CreateSet(string setName, string setId)
        {
            var set = new SetEntry
                {
                    Name = setName,
                    SetId = setId
                };

            using (var work = _store.Begin())
                work.Add(set);

            _cache.SetsByName[set.Name] = set;
            _cache.SetsBySetId[set.SetId] = set;

            return set;
        }

        public SetEntry GetSet(string setName)
        {
            return _cache.SetsByName[setName];
        }

        public SetEntry GetSetByFlickrSetId(string photosetId)
        {
            return _cache.SetsBySetId[photosetId];
        }

        public void UpdateSet(SetEntry setEntry)
        {
            using (var work = _store.Begin())
                work.Update(setEntry);

            _cache.SetsByName[setEntry.Name] = setEntry;
            _cache.SetsBySetId[setEntry.SetId] = setEntry;
        }

        public void SkipEntry(FileEntry entry)
        {
            entry.Skip = true;
            using (var work = _store.Begin())
                work.Update(entry);
        }

        public FileSetLink LinkEntryToSet(int entryId, int setId)
        {
            using (var work = _store.Begin())
            using (var trans = work.BeginTransaction())
            {
                var key = string.Join("-", entryId, setId);

                var link = _cache.FileSetLinkByIds[key]; //_store.FirstOrDefault<FileSetLink>(x => x.FileEntryId == entryId && x.SetId == setId);
                if (link != null)
                    return link;

                link = new FileSetLink
                    {
                        FileEntryId = entryId,
                        SetId = setId
                    };

                work.Add(link);
                _cache.FileSetLinkByIds[key] = link;

                trans.Commit();

                return link;
            }
        }

        public StoreWork Begin()
        {
            return _store.Begin();
        }

        public void Compact(bool full = false)
        {
            _store.Compact(full);
        }

        public void UpdateEntrySynced(int id, DateTime timestamp)
        {
            using (var work = _store.Begin())
                work.Execute("UPDATE FileEntry SET Synced = ? WHERE Id = ?", timestamp, id);
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            if (_store != null)
                _store.Dispose();
        }

        #endregion
    }
}