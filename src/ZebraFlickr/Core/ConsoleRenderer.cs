﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;

namespace ZebraFlickr.Core
{
	public static class ConsoleRenderer
	{
		public static void OverwriteConsoleMessage(string message)
		{
			Console.CursorLeft = 0;


			if (string.IsNullOrEmpty(message))
				return;

			int maxCharacterWidth = Console.WindowWidth - 1;
			if (message.Length > maxCharacterWidth)
			{
				message = message.Substring(0, maxCharacterWidth - 3) + "...";
			}
			message = message + new string(' ', maxCharacterWidth - message.Length);

			Console.Write(message);
		}

        public static void RenderConsoleProgress(int percentage,
	                                      string name,
	                                      int bytesSent,
	                                      int totalBytes,
                                          DateTime startTime,
                                          string operation = "uploaded",
	                                      char leftCharacter = '[',
	                                      char rightCharacter = ']')
	    {
            RenderConsoleProgress(percentage, name, bytesSent, totalBytes, DateTime.Now.Subtract(startTime), operation, leftCharacter, rightCharacter);
	    }

        public static void RenderConsoleProgress(int percentage,
                                          string name,
                                          int bytesSent,
                                          int totalBytes,
                                          TimeSpan time,
                                          string operation = "uploaded",
                                          char leftCharacter = '[',
                                          char rightCharacter = ']')
        {
            var bytesPerSecond = bytesSent / time.TotalMilliseconds * 1000;

            RenderConsoleProgress(percentage,
                                  '■',
                                  ConsoleColor.Green,
                                  string.Format("{0}%. {2} / second. {3} {4} of {5}. {1}",
                                                percentage,
                                                name,
                                                bytesPerSecond.ToFormattedByteCountString(2),
                                                bytesSent.ToFormattedByteCountString(2),
                                                operation,
                                                totalBytes.ToFormattedByteCountString(2)));
        }

		public static void RenderConsoleProgress(int percentage,
		                                         char progressBarCharacter,
		                                         ConsoleColor color,
		                                         string message,
		                                         char leftCharacter = '[',
		                                         char rightCharacter = ']')
		{
			lock (Console.Out)
			{
				try
				{
					ConsoleColor originalColor = Console.ForegroundColor;
					Console.ForegroundColor = color;
					Console.CursorLeft = 0;
					int width = Console.WindowWidth - 3;
					var newWidth = (int) ((width*percentage)/100d);
					string progBar = leftCharacter + new string(progressBarCharacter, newWidth) +
					                 new string(' ', Math.Abs(width - newWidth)) + rightCharacter;
					Console.Write(progBar);
					if (string.IsNullOrEmpty(message))
						message = "";
					Console.CursorTop++;

					if (Console.CursorTop > Console.BufferHeight)
						Console.WriteLine();

					OverwriteConsoleMessage(message);
					Console.CursorTop--;
					Console.ForegroundColor = originalColor;
				}
				catch (Exception)
				{
				}
			}
		}

		public static void EnsureConsoleSpace()
		{
			Console.WriteLine();
			Console.WriteLine();
			Console.CursorTop -= 2;
		}
	}
}