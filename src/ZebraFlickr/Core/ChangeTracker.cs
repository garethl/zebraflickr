﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections;
using LinFu.DynamicProxy;

namespace ZebraFlickr.Core
{
    public class ChangeTracker<T> : IDisposable, IInvokeWrapper
        where T : class
    {
        private static readonly ProxyFactory _proxyFactory = new ProxyFactory();

        private T _proxy;
        private T _value;

        public ChangeTracker(T value)
        {
            _value = value;
            _proxy = _proxyFactory.CreateProxy<T>(new CallAdapter(this));
        }

        public T Wrapped
        {
            get { return _proxy; }
        }

        #region Implementation of IInvokeWrapper

        public void BeforeInvoke(InvocationInfo info)
        {
            if (info.TargetMethod.Name.StartsWith("set_") && info.Arguments.Length == 1)
            {
                var result = typeof (T).GetMethod("get_" + info.TargetMethod.Name.Substring(4)).Invoke(_value, null);

                if (result is Array)
                {
                    var a = (object[])info.Arguments[0];
                    var b = (object[])result;

                    if (a.Length == b.Length)
                    {
                        for (int x = 0; x < a.Length; x++)
                        {
                            if (Comparer.Default.Compare(a[x], b[x]) != 0)
                            {
                                if (Changed != null)
                                    Changed();

                                return;
                            }
                        }

                        return;
                    }

                    if (Changed != null)
                        Changed();
                }
                else if (Comparer.Default.Compare(info.Arguments[0], result) != 0)
                {
                    if (Changed != null)
                        Changed();
                }
            }
        }

        public object DoInvoke(InvocationInfo info)
        {
            return info.TargetMethod.Invoke(_value, info.Arguments);
        }

        public void AfterInvoke(InvocationInfo info, object returnValue)
        {
            //nothing
        }

        #endregion

        public event Action Changed;

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            _proxy = null;
            _value = null;
        }

        #endregion
    }
}