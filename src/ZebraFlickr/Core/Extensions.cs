﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Globalization;
using System.Text;

namespace ZebraFlickr.Core
{
	public static class Extensions
	{
		/// <summary>
		/// Hex lookup
		/// </summary>
		private static readonly char[] HexLookup = new[]
			{
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
			};

		/// <summary>
		/// Converts a byte array to a hex string
		/// </summary>
		/// <param name="bytes">Bytes to convert</param>
		/// <returns>Hex representation of the byte array</returns>
		public static string ToHex(this byte[] bytes)
		{
			var buffer = new StringBuilder(bytes.Length*2);
			int length = bytes.Length;
			for (int i = 0; i < length; i++)
			{
				buffer.Append(HexLookup[bytes[i] >> 4]);
				buffer.Append(HexLookup[bytes[i] & 15]);
			}
			return buffer.ToString();
		}

		public static string ToHex(this byte[] bytes, int width)
		{
			var buffer = new StringBuilder(bytes.Length*2);
			int length = bytes.Length;
			for (int i = 0; i < length; i++)
			{
				buffer.Append(HexLookup[bytes[i] >> 4]);
				buffer.Append(HexLookup[bytes[i] & 15]);

				if (i > 0 && i%width == 0)
					buffer.AppendLine();
			}

			return buffer.ToString();
		}

		/// <summary>
		/// Converts a hex string to a byte array
		/// </summary>
		/// <param name="hexString">Hex string to convert</param>
		/// <returns>Byte array</returns>
		public static byte[] FromHex(this string hexString)
		{
			if (hexString.Length%2 != 0)
				throw new Exception("Expected string of even length");

			var ret = new byte[hexString.Length/0x2];
			for (var i = 0x0; i < (hexString.Length/0x2); i++)
			{
				ret[i] = (byte) ((ToByte(hexString[i*0x2])*0x10) + ToByte(hexString[(i*0x2) + 0x1]));
			}

			return ret;
		}

		private static byte ToByte(char c)
		{
			return byte.Parse(c.ToString(), NumberStyles.HexNumber, CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Returns true if this exception of any child exceptions are of type (T)
		/// </summary>
		public static bool Is<T>(this Exception ex) where T : Exception
		{
			while (ex != null)
			{
				if (ex is T)
					return true;

				ex = ex.InnerException;
			}

			return false;
		}
	}
}