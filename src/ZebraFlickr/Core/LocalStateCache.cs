#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Core
{
    public class LocalStateCache
    {
        public LocalStateCache(PersistentStore store)
        {
            FilesByName = new MemoryCache<string, FileEntry>(StringComparer.OrdinalIgnoreCase);
            FilesByHash = new MemoryCache<byte[], FileEntry>(ByteArrayComparer.Instance);
            FilesByPhotoId = new MemoryCache<string, FileEntry>(StringComparer.OrdinalIgnoreCase);

            SetsByName = new MemoryCache<string, SetEntry>(StringComparer.OrdinalIgnoreCase);
            SetsBySetId = new MemoryCache<string, SetEntry>(StringComparer.OrdinalIgnoreCase);

            FileSetLinkByIds = new MemoryCache<string, FileSetLink>();

            Load(store);
        }

        public MemoryCache<string, FileEntry> FilesByName { get; set; }
        public MemoryCache<byte[], FileEntry> FilesByHash { get; set; }
        public MemoryCache<string, FileEntry> FilesByPhotoId { get; set; } 

        public MemoryCache<string, SetEntry> SetsByName { get; set; }
        public MemoryCache<string, SetEntry> SetsBySetId { get; set; }
        public MemoryCache<string, FileSetLink> FileSetLinkByIds { get; set; }

        private void Load(PersistentStore store)
        {
            using (var work = store.Begin())
            {
                foreach (var file in work.Get<FileEntry>())
                {
                    FilesByName[file.Name] = file;
                    FilesByHash[file.Hash] = file;

                    if (!string.IsNullOrEmpty(file.PictureId))
                        FilesByPhotoId[file.PictureId] = file;
                }

                foreach (var set in work.Get<SetEntry>())
                {
                    SetsByName[set.Name] = set;
                    SetsBySetId[set.SetId] = set;
                }

                foreach (var link in work.Get<FileSetLink>())
                {
                    FileSetLinkByIds[string.Join("-", link.FileEntryId, link.SetId)] = link;
                }
            }

            Log.Debug("Found {0} tracked files", FilesByName.Count);
            Log.Debug("Found {0} tracked sets", SetsByName.Count);
        }

        public void Deleted(FileEntry entry)
        {
            FilesByName.Remove(entry.Name);
            FilesByHash.Remove(entry.Hash);
            FilesByPhotoId.Remove(entry.PictureId);
        }
    }
}