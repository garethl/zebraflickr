﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;

namespace ZebraFlickr.Core
{
	/// <summary>
	/// Byte array equality comparer
	/// </summary>
	public class ByteArrayComparer : IEqualityComparer<byte[]>, IComparer<byte[]>
	{
		private static readonly ByteArrayComparer _instance = new ByteArrayComparer();

		public static ByteArrayComparer Instance
		{
			get { return _instance; }
		}

		#region IComparer<byte[]> Members

		/// <summary>
		/// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
		/// </summary>
		/// <returns>
		/// Less than zero <paramref name="x"/> is less than <paramref name="y"/>.
		/// Zero <paramref name="x"/> equals <paramref name="y"/>.
		/// Greater than zero <paramref name="x"/> is greater than <paramref name="y"/>.
		/// </returns>
		/// <param name="x">The first object to compare.</param>
		/// <param name="y">The second object to compare.</param>
		public int Compare(byte[] x, byte[] y)
		{
			for (int i = 0; i < x.Length && i < y.Length; ++i)
			{
				int c = x[i] - y[i];
				if (c != 0)
					return c;
			}

			if (x.Length < y.Length)
				return -1;

			if (x.Length > y.Length)
				return 1;

			return 0;
		}

		#endregion

		#region IEqualityComparer<byte[]> Members

		/// <summary>
		/// Determines whether the specified objects are equal.
		/// </summary>
		/// <returns>
		/// true if the specified objects are equal; otherwise, false.
		/// </returns>
		/// <param name="x">The first objectto compare.</param>
		/// <param name="y">The second object to compare.</param>
		public bool Equals(byte[] x, byte[] y)
		{
			if (x == null || y == null || x.Length != y.Length)
				return false;

			for (var i = 0; i < x.Length; i++)
			{
				if (x[i] != y[i])
					return false;
			}

			return true;
		}

		/// <summary>
		/// Returns a hash code for the specified object.
		/// </summary>
		/// <returns>
		/// A hash code for the specified object.
		/// </returns>
		/// <param name="obj">The <see cref="T:System.Object"/> for which a hash code is to be returned.</param>
		/// <exception cref="T:System.ArgumentNullException">The type of <paramref name="obj"/> is a reference type and <paramref name="obj"/> is null.</exception>
		public int GetHashCode(byte[] obj)
		{
			unchecked
			{
				const int p = 16777619;
				var hash = (int) 2166136261;

				for (var i = 0; i < obj.Length; i++)
					hash = (hash ^ obj[i])*p;

				hash += hash << 13;
				hash ^= hash >> 7;
				hash += hash << 3;
				hash ^= hash >> 17;
				hash += hash << 5;
				return hash;
			}
		}

		#endregion

		public int Compare(byte[] x, int offset, int count, byte[] y)
		{
			if (count != y.Length)
				throw new ArgumentOutOfRangeException("Count does not match second argument's length");

			var end = offset + count;
			for (int i = offset; i < end; ++i)
			{
				int c = x[i] - y[i - offset];
				if (c != 0)
					return c;
			}

			return 0;
		}
	}
}