#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Linq;
using System.Threading;
using FlickrNet;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Core.Processing
{
	public class FlickrSetAssociator
	{
		private readonly Flickr _flickr;
		private readonly LocalState _state;

		public FlickrSetAssociator(LocalState state, UserDetails details)
		{
			_state = state;
			_flickr = FlickrClient.Create(details);
		}

		#region Implementation of IPipelineProcess<FileEntry>

        public FileEntry Execute(FileEntry entry)
		{
			if (entry.Skip)
				return entry;

			if (entry.Uploaded && !entry.UploadedSet && !string.IsNullOrEmpty(entry.PictureId))
			{
				var parts = entry.Name.Split(new[] {'\\', '/'});
				var setName = string.Join(@"\", parts.Skip(_state.Config.DirectoryLevelsToIgnore).Take(parts.Length - 1 - _state.Config.DirectoryLevelsToIgnore));

                if (string.IsNullOrEmpty(setName))
                {
                    if (parts.Length >= 2)
                    {
                        setName = parts[parts.Length - 2];
                    }
                }

                if (string.Equals(setName, "Not in set", StringComparison.InvariantCultureIgnoreCase))
                    return entry;

				Log.Info("Set: Associating {0} to {1}", entry.Name, setName);

				if (string.IsNullOrEmpty(setName))
				{
					setName = "Unorganised";
				}

				var set = _state.GetSet(setName);

				int count = 0;
				while (true)
				{
					try
					{
						if (set == null)
						{
							var id = _flickr.PhotosetsCreate(setName, entry.PictureId).PhotosetId;
							set = _state.CreateSet(setName, id);
						}
						else
						{
							_flickr.PhotosetsAddPhoto(set.SetId, entry.PictureId);
						}

						entry.UploadedSet = true;

						_state.UpdateEntry(entry);
						_state.LinkEntryToSet(entry.Id, set.Id);

						return entry;
					}
					catch (ThreadAbortException)
					{
						//nothing
					}
					catch (FlickrApiException ex)
					{
						if (CancelHandler.Cancelled)
							break;

                        if (ex.Code == 3)
                        {
                            //photo already in set
                            entry.UploadedSet = true;
                            _state.UpdateEntry(entry);
                            _state.LinkEntryToSet(entry.Id, set.Id);

                            return entry;
                        }

						Log.Error("Error received from flickr. Skipping file and continuing", ex);
						_state.SkipEntry(entry);
						break;
					}
					catch (Exception ex)
					{
						if (CancelHandler.Cancelled)
							break;

						Console.WriteLine();
						Console.WriteLine();

						Log.Error("Error associating with set {0}. Trying again", ex, entry.Name);
						count++;

						if (count >= 3)
						{
							Log.Error("Attempted 3 times to upload, skipping this file temporarily.");
							return entry;
						}
					}
				}
			}
			else
			{
				Log.Debug("Set: Ignoring file {0}", entry.Name);
			}

            return entry;
		}

		#endregion
	}
}