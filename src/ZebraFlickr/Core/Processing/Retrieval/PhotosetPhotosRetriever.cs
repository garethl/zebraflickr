﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlickrNet;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Core.Processing.Retrieval
{
    public class PhotosetPhotosRetriever
    {
        private const PhotoSearchExtras SearchFlags =
            PhotoSearchExtras.Tags
            | PhotoSearchExtras.MachineTags
            | PhotoSearchExtras.OriginalUrl
            | PhotoSearchExtras.Usage
            | PhotoSearchExtras.OriginalFormat
            | PhotoSearchExtras.Media
            | PhotoSearchExtras.DateTaken
            | PhotoSearchExtras.DateUploaded;

        private const int PageSize = 500;

        private readonly Flickr _flickr;
        private readonly TimeSpan _maxCacheAge;
        private readonly LocalState _state;

        public PhotosetPhotosRetriever(LocalState state, UserDetails details, TimeSpan maxCacheAge)
        {
            _state = state;
            _maxCacheAge = maxCacheAge;
            _flickr = FlickrClient.Create(details);
        }

        /// <summary>
        /// Run the transformation
        /// </summary>
        /// <param name="event">Source to convert</param>
        /// <returns>The converted results</returns>
        public IGrouping<SetEntry, Photo> Execute(Tuple<SetEntry, Photoset> t)
        {
            var set = t.Item1;
            var photoSet = t.Item2;

            Log.Info("Retrieving photos for set '{0}' from flickr...", set.Name);

            var key = "Flickr_Photos_" + set.SetId;
            var photos = _state.Cache.GetIfYoungerThan<List<Photo>>(_maxCacheAge, key);

            if (photos == null)
            {
                if (photoSet == null)
                {
                    photos = new List<Photo>();

                    bool more = true;

                    while (more)
                    {
                        for (int page = 1; page < int.MaxValue; page++)
                        {
                            var values = Try.Execute(5, () => _flickr.PhotosGetNotInSet(page, PageSize, SearchFlags));

                            if (values == null || values.Count == 0)
                            {
                                more = false;
                                break;
                            }

                            photos.AddRange(values);
                        }
                    }
                }
                else
                {
                    int expected = photoSet.NumberOfPhotos + photoSet.NumberOfVideos;
                    var pages = (int) Math.Ceiling(expected/(decimal) PageSize);

                    photos = new List<Photo>(expected);

                    for (int page = 1; page <= pages; page++)
                    {
                        var values = Try.Execute(5, () => _flickr.PhotosetsGetPhotos(set.SetId, SearchFlags, page, PageSize));

                        if (values == null || values.Count == 0)
                            break;

                        photos.AddRange(values);
                    }
                }
            }

            Task.Factory.StartNew(() => _state.Cache.Set(key, photos));

            return new ManualGrouping<SetEntry, Photo>(set, photos);
        }
    }
}