﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using FlickrNet;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Core.Processing.Retrieval
{
    public class PhotoHandler
    {
        private readonly LocalState _state;
        private readonly string _path;
        private Flickr _flickr;

        public PhotoHandler(LocalState state, UserDetails details, string path)
        {
            _state = state;
            _path = path;
            _flickr = FlickrClient.Create(details);
        }

        public void Handle(IGrouping<SetEntry, Photo> set)
        {
            using (var work = _state.Begin())
            //using (var trans = work.BeginTransaction())
            {
                int count = 0;

                foreach (var photo in set)
                {
                    Handle(set.Key, photo);
                    count++;
                }

                _state.UpdateSet(set.Key);

                //trans.Commit();

                Log.Info("Processed {0} photos in set {1}", count, set.Key.Name);
            }
        }

        private void Handle(SetEntry set, Photo photo)
        {
            Log.Debug("Processing photo {0}: {1} in set {2}: {3}", photo.PhotoId, photo.Title, set.SetId, set.Name);

            var entry = _state.Lookup.FilesByPhotoId[photo.PhotoId];

            if (entry != null)
            {
                ProcessMatchedFile(set, photo, entry);
            }
            else
            {
                var hashTag = GetHashTag(photo);

                if (hashTag != null)
                {
                    hashTag = hashTag.Substring(9);
                    var parts = hashTag.Split('=');

                    if (parts.Length == 2)
                    {
                        ProcessPhotoWithHash(set, photo, parts[0], parts[1]);
                        return;
                    }
                }

                ProcessPhotoWithoutHash(set, photo);
            }
        }

        private void ProcessPhotoWithHash(SetEntry setEntry, Photo photo, string hashType, string hashValue)
        {
            if (hashType.Equals("sha1", StringComparison.OrdinalIgnoreCase))
            {
                using (var work = _state.Begin())
                using (var trans = work.BeginTransaction())
                {
                    if (hashValue.Length >= 40)
                    {
                        hashValue = hashValue.Substring(0, 40);

                        //see if we have the photo
                        var entry = _state.GetEntryByHash(hashValue);
                        if (entry != null)
                        {
                            ProcessMatchedFile(setEntry, photo, entry);
                            trans.Commit();

                            return;
                        }
                    }
                }
            }
            
            //meh
            ProcessPhotoWithoutHash(setEntry, photo);
        }

        private void ProcessPhotoWithoutHash(SetEntry set, Photo photo)
        {
            Log.Info("Found photo on Flickr that doesn't match a local photo or has no hash. {0}: {1}", photo.PhotoId, photo.Title);

            if (photo.CanDownload != null && photo.CanDownload.Value)
            {
                Log.Info("Downloading photo to calculate hash...");

                var tempFile = TempStore.Open(photo.PhotoId);

                if (!tempFile.Exists) //download it
                {
                    using (var file = tempFile.OpenWrite())
                    {
                        if (!DownloadPhoto(photo, file.FileName))
                            return;
                        file.Commit();
                    }
                }

                byte[] hash;
                using (var read = tempFile.OpenRead())
                    hash = HashCalculator.CalculateHash(read);

                var entry = _state.GetEntryByHash(hash);

                if (entry != null)
                {
                    Log.Info("Matched flickr image with local image {0}", entry.Name);

                    ProcessMatchedFile(set, photo, entry);
                    return;
                }

                Log.Info("Unable to match flickr image with any local image.");
                ProcessNewDownloadedPhoto(set, photo, hash);
                return;
            }
            
            Log.Warn("Flickr says we cannot download photo {0}: {1}, so have no way of identifying it. Skipping.", photo.PhotoId, photo.Title);
        }

        private void ProcessNewDownloadedPhoto(SetEntry set, Photo photo, byte[] hash)
        {
            Log.Info("Importing new photo into library: {0}\\{1}", set.Name, photo.Title);

            var setFolder = set.Name.ReplaceInvalidFileChars("_");
            var fileName = photo.Title.ReplaceInvalidFileChars("_");

            if (!fileName.Contains("."))
                fileName += "." + photo.OriginalFormat;

            //copy the temporary file to the new place
            var folder = Path.Combine(_path, setFolder);
            var file = Path.Combine(folder, fileName);

            if (File.Exists(file))
            {
                Log.Warn("Unable to import downloaded image from flickr, as it exists on disk already. Skipping");
                return;
            }

            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            var tempFile = TempStore.Open(photo.PhotoId);

            var entry = new FileEntry()
                {
                    Name = setFolder + @"\" + fileName,
                    Hash = hash,
                    Modified = File.GetLastWriteTime(tempFile.FullFilePath),
                    Size = new FileInfo(tempFile.FullFilePath).Length
                };

            tempFile.MoveTo(file);

            ProcessMatchedFile(set, photo, entry, true);
        }

        private void ProcessMatchedFile(SetEntry setEntry, Photo photo, FileEntry entry, bool isNewEntry = false)
        {
            bool changed = isNewEntry;

            using (var track = new ChangeTracker<FileEntry>(entry))
            {
                track.Changed += () =>
                    {
                        changed = true;
                    };

                var p = track.Wrapped;

                p.PictureId = photo.PhotoId;
                p.Uploaded = true;

                if (setEntry.Id != 0)
                    p.UploadedSet = true;
                p.Skip = false;
                p.ContentChanged = false;
                p.UploadedMetaData = true;

                p.FlickrTags = photo.Tags.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                var expectedTags = TagCreator.Create(entry, true);
                var missingTags = expectedTags.Except(entry.FlickrTags, StringComparer.OrdinalIgnoreCase).ToArray();//expectedTags.Where(x => !entry.FlickrTags.Contains(x, StringComparer.OrdinalIgnoreCase)).ToArray();
                if (missingTags.Length > 0)
                    p.MetaDataChanged = true;
            }

            if (changed)
            {
                Log.Warn("Changed: {0}: {1}", entry.PictureId, entry.Name);
                entry.Synced = DateTime.UtcNow;

                if (isNewEntry)
                    _state.AddEntry(entry);
                else
                    _state.UpdateEntry(entry);

                if (setEntry.Id != 0)
                    _state.LinkEntryToSet(entry.Id, setEntry.Id);
            }
            else
            {
                entry.Synced = DateTime.UtcNow;
                _state.UpdateEntrySynced(entry.Id, entry.Synced.Value);
            }

            Log.Debug("Matched with local file " + entry.Name);
        }

        private bool DownloadPhoto(Photo photo, string file)
        {
            string url = photo.OriginalUrl;

            if (photo.Media.Equals("video", StringComparison.InvariantCultureIgnoreCase))
            {
                var sizes = _flickr.PhotosGetSizes(photo.PhotoId);

                var size = sizes.FirstOrDefault(x => x.Label.Equals("Video Original", StringComparison.InvariantCultureIgnoreCase));

                if (size == null)
                {
                    Log.Warn("Unable to find video url from flickr for photo {0}: {1}", photo.PhotoId, photo.Title);
                    return false;
                }

                url = size.Source;
            }

            var startTime = DateTime.Now;
            using (var client = new WebClient())
            {
                AsyncCompletedEventArgs result = null;
                using (var mre = new ManualResetEvent(false))
                {
                    client.DownloadProgressChanged += (sender, e) => ConsoleRenderer.RenderConsoleProgress(e.ProgressPercentage, photo.Title, (int) e.BytesReceived, (int) e.TotalBytesToReceive, startTime, "downloaded");
                    client.DownloadFileCompleted += (sender, args) =>
                        {
                            result = args;
                            mre.Set();
                        };

                    client.DownloadFileAsync(new Uri(url), file);

                    mre.WaitOne();

                    if (result == null || result.Cancelled || result.Error != null)
                        throw new Exception("Error downloading", result == null ? null : result.Error);

                    ConsoleRenderer.RenderConsoleProgress(100, '■', ConsoleColor.Green, "");
                    Console.WriteLine();
                    Console.WriteLine();
                }
            }

            //tweak the timestamps on the file
            File.SetCreationTime(file, photo.DateTaken.ToLocalTime());
            File.SetLastWriteTime(file, photo.DateUploaded.ToLocalTime());

            return true;
        }

        private static string GetHashTag(Photo photo)
        {
            var hashTags = photo.Tags.Where(x => x.StartsWith("checksum:", StringComparison.OrdinalIgnoreCase)).ToArray();

            string hashTag = null;

            if (hashTags.Length == 1)
            {
                hashTag = hashTags[0];
            }
            else
            {
                var sha1 = hashTags.FirstOrDefault(x => x.StartsWith("checksum:sha1="));

                if (sha1 == null)
                {
                    var md5 = hashTags.FirstOrDefault(x => x.StartsWith("checksum:md5="));

                    if (md5 != null)
                    {
                        hashTag = md5;
                    }
                }
                else
                {
                    hashTag = sha1;
                }
            }

            return hashTag;
        }
    }
}