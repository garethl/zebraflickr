﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlickrNet;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Core.Processing.Retrieval
{
    public class PhotosetRetriever : IEnumerable<Tuple<SetEntry, Photoset>>
    {
        private readonly Flickr _flickr;
        private readonly TimeSpan _maxCacheAge;
        private readonly LocalState _state;

        public PhotosetRetriever(LocalState state, UserDetails details, TimeSpan maxCacheAge)
        {
            _state = state;
            _maxCacheAge = maxCacheAge;

            _flickr = FlickrClient.Create(details);
        }

        #region Implementation of IEnumerable

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        /// <filterpriority>1</filterpriority>
        public IEnumerator<Tuple<SetEntry, Photoset>> GetEnumerator()
        {
            Log.Info("Retrieving photo sets from flickr...");

            var items = Try.Execute(5,
                               delegate
                                   {
                                       var key = "Flickr_Photosets";
                                       var photoSets = _state.Cache.GetIfYoungerThan<PhotosetCollection>(_maxCacheAge, key);

                                       if (photoSets != null)
                                           return photoSets;

                                       photoSets = _flickr.PhotosetsGetList();

                                       Task.Factory.StartNew(() => _state.Cache.Set(key, photoSets));

                                       return photoSets;
                                   });

            var sortedItems = items.OrderBy(x => x.Title);

            foreach (var item in sortedItems)
            {
                var set = _state.GetSetByFlickrSetId(item.PhotosetId) 
                    ?? _state.CreateSet(item.Title, item.PhotosetId);

                //if it hasn't changed since we last synced it, ignore
                if (set.Synced != null && set.Synced.Value >= item.DateUpdated)
                    continue;

                set.Synced = DateTime.UtcNow;

                yield return Tuple.Create(set, item);
            }

            var noSet = _state.GetSetByFlickrSetId("-1") ?? new SetEntry
                {
                    Name = "Not in set",
                    SetId = "-1"
                };

            noSet.Synced = DateTime.Now;

            yield return Tuple.Create(noSet, (Photoset)null);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}