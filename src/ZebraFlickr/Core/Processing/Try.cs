using System;

namespace ZebraFlickr.Core.Processing
{
    public class Try
    {
        public static void Execute(int times, Action action, string errorMessage = "Error executing")
        {
            Execute<object>(times,
                            () =>
                                {
                                    action();
                                    return null;
                                }
                            ,
                            errorMessage);
        }

        public static T Execute<T>(int times, Func<T> action, string errorMessage = "Error executing")
        {
            int attempts = 0;

            TRY_AGAIN:
            try
            {
                return action();
            }
            catch (Exception ex)
            {
                Log.Error(errorMessage, ex);
                attempts++;

                if (attempts < times)
                {
                    Log.Info("Trying again...");
                    goto TRY_AGAIN;
                }

                throw;
            }
        }
    }
}