#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Linq;
using System.Threading;
using FlickrNet;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Core.Processing
{
	public class FlickrMetaDataSetter
	{
		private readonly Flickr _flickr;
		private readonly LocalState _state;

		public FlickrMetaDataSetter(LocalState state, UserDetails details)
		{
			_state = state;
			_flickr = FlickrClient.Create(details);
		}

        public FileEntry Execute(FileEntry entry)
		{
			if (entry.Skip)
				return entry;

			if (entry.Uploaded && !string.IsNullOrEmpty(entry.PictureId))
			{
				if (!entry.UploadedMetaData || entry.MetaDataChanged)
				{
					Log.Info("Metadata: Setting tags on {0}", entry.Name);

					var tags = TagCreator.Create(entry);

					int count = 0;
					while (true)
					{
						try
						{
							_flickr.PhotosAddTags(entry.PictureId, tags.Select(x => string.Concat("\"", x, "\"")).ToArray());

							entry.UploadedMetaData = true;
							entry.MetaDataChanged = false;
							_state.UpdateEntry(entry);

							return entry;
						}
						catch (ThreadAbortException)
						{
							//nothing
						}
						catch (FlickrApiException ex)
						{
							if (CancelHandler.Cancelled)
								break;

							Console.WriteLine();
							Console.WriteLine();

							Log.Error("Error received from flickr. Skipping file and continuing", ex);
							_state.SkipEntry(entry);
							break;
						}
						catch (Exception ex)
						{
							if (CancelHandler.Cancelled)
								break;

							Console.WriteLine();
							Console.WriteLine();

							Log.Error("Error setting meta data on {0}. Trying again", ex, entry.Name);
							count++;

							if (count >= 3)
							{
								Log.Error("Attempted 3 times to upload, skipping this file temporarily.");
								return entry;
							}
						}
					}
				}
			}

			Log.Debug("Metadata: Ignoring file {0}", entry.Name);
            return entry;
		}
	}
}