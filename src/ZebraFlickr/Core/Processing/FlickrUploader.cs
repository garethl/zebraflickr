﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using FlickrNet;
using ZebraFlickr.Core.Store;

namespace ZebraFlickr.Core.Processing
{
	public class FlickrUploader
	{
		private readonly Flickr _flickr;
		private readonly string _root;
		private readonly LocalState _state;
		private string _currentFile;
		private Stopwatch _currentTime;

		public FlickrUploader(string root, LocalState state, UserDetails details)
		{
			_root = root;
			_state = state;

			_flickr = FlickrClient.Create(details);
			_flickr.OnUploadProgress += OnFlickrOnOnUploadProgress;
		}

		private void OnFlickrOnOnUploadProgress(object sender, UploadProgressEventArgs e)
		{
			var bytesPerSecond = e.BytesSent/(double) _currentTime.ElapsedMilliseconds*1000;

			if (CancelHandler.Cancelled)
				throw new OperationCanceledException();

            ConsoleRenderer.RenderConsoleProgress(e.ProcessPercentage, _currentFile, (int)e.BytesSent, (int)e.TotalBytesToSend, _currentTime.Elapsed);
		}

        public FileEntry Execute(FileEntry entry)
		{
			if (entry.Uploaded)
			{
				Log.Debug("Upload: Ignoring file {0}", entry.Name);
				return entry;
			}

            if (entry.Skip)
            {
                Log.Warn("Skipping previously errored file {0}", entry.Name);
                return entry;
            }

			Log.Info("Upload: Uploading {0}", entry.Name);

			var file = Path.Combine(_root, entry.Name);

			var tags = TagCreator.Create(entry);

			int count = 0;

			while (true)
			{
				try
				{
					_currentFile = entry.Name;
					_currentTime = Stopwatch.StartNew();

					ConsoleRenderer.EnsureConsoleSpace();

					var pictureId = _flickr.UploadPicture(file,
					                                      null,
					                                      null,
					                                     string.Join(",", tags),
					                                      _state.Config.Public,
					                                      _state.Config.Family,
					                                      _state.Config.Friend);

					_currentTime.Stop();

                    ConsoleRenderer.RenderConsoleProgress(100, '■', ConsoleColor.Green, "");

					Console.WriteLine();
					Console.WriteLine();

					entry.PictureId = pictureId;
					entry.Uploaded = true;
					entry.UploadedMetaData = true;
					entry.ContentChanged = false;
					entry.MetaDataChanged = false;
					_state.UpdateEntry(entry);

					var totalBytes = new FileInfo(file).Length;
					var bytesPerSecond = totalBytes/(double) _currentTime.ElapsedMilliseconds*1000;

					Log.Info("Upload: Completed {0}. {1} / second. {2} in total.", entry.Name, bytesPerSecond.ToFormattedByteCountString(2), totalBytes.ToFormattedByteCountString(2));

                    return entry;
				}
				catch (ThreadAbortException)
				{
					//nothing
					break;
				}
				catch (FlickrApiException ex)
				{
					if (CancelHandler.Cancelled)
						break;

					Console.WriteLine();
					Console.WriteLine();

					Log.Error("Error received from flickr. Skipping file and continuing", ex);
					_state.SkipEntry(entry);
					break;
				}
				catch (Exception ex)
				{
					if (CancelHandler.Cancelled)
						break;

					Console.WriteLine();
					Console.WriteLine();

					Log.Error("Error uploading {0}. Trying again", ex, entry.Name);
					count++;

					if (count >= 3)
					{
						Log.Error("Attempted 3 times to upload, skipping this file temporarily.");
						return entry;
					}
				}
				finally
				{
					_currentTime.Stop();
				}
			}

            return entry;
		}
	}
}