﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace ZebraFlickr.Core
{
    public static class Pipeline
    {
        public static ISourceBlock<T> Source<T>(IEnumerable<T> items)
        {
            var block = new BufferBlock<T>();

            Task.Run(() =>
                {
                    foreach (var item in items)
                    {
                        block.Post(item);
                    }

                    block.Complete();
                });

            return block;
        }

        public static ISourceBlock<TOut> Process<TIn, TOut>(this ISourceBlock<TIn> source, Func<TIn, TOut> func, ExecutionDataflowBlockOptions options = null)
        {
            if (options == null)
                options = new ExecutionDataflowBlockOptions();

            var block = new TransformBlock<TIn, TOut>(func, options);

            Link(source, block);

            return block;
        }

        private static void Link<T>(ISourceBlock<T> source, ITargetBlock<T> block, DataflowLinkOptions options = null)
        {
            if (options == null)
                options = new DataflowLinkOptions();

            source.LinkTo(block, options);
            source.Completion.ContinueWith(t =>
                {
                    if (t.IsFaulted)
                        block.Fault(t.Exception);
                    else
                        block.Complete();
                });
        }

        public static ISourceBlock<TOut> Process<TIn, TOut>(this ISourceBlock<TIn> source, Func<TIn, IEnumerable<TOut>> func, ExecutionDataflowBlockOptions options = null)
        {
            if (options == null)
                options = new ExecutionDataflowBlockOptions();

            var block = new TransformManyBlock<TIn, TOut>(func, options);

            Link(source, block);

            return block;
        }

        public static ITargetBlock<T> Consume<T>(this ISourceBlock<T> source, Action<T> func, ExecutionDataflowBlockOptions options = null)
        {
            if (options == null)
                options = new ExecutionDataflowBlockOptions();

            var block = new ActionBlock<T>(func, options);

            Link(source, block);

            return block;
        }

        public static void Wait<T>(this ITargetBlock<T> target, CancellationToken token)
        {
            target.Completion.Wait(token);
        }
    }
}