using System;
using SQLite;

namespace ZebraFlickr.Core.Store
{
    public class CacheEntry
    {
        [PrimaryKey]
        public string Key { get; set; }

        public DateTime DateModified { get; set; }

        public string Value { get; set; }

        public string Type { get; set; }
    }
}