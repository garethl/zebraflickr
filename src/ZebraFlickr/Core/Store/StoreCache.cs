﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.Text;

namespace ZebraFlickr.Core.Store
{
    public class StoreCache
    {
        private readonly PersistentStore _store;

        public StoreCache(PersistentStore store)
        {
            _store = store;
        }

        public T Get<T>(string key)
        {
            using (var work = _store.Begin())
            {
                var entry = work.Get<CacheEntry>(x => x.Key == key).FirstOrDefault();

                if (entry == null)
                    return default(T);

                return (T) JsonSerializer.DeserializeFromString(entry.Value, typeof (T));
            }
        }

        public T GetIfYoungerThan<T>(TimeSpan age, string key)
        {
            using (var work = _store.Begin())
            {
                var entry = work.Get<CacheEntry>(x => x.Key == key).FirstOrDefault();

                if (entry == null)
                    return default(T);

                if (entry.DateModified.Add(age) < DateTime.UtcNow)
                {
                    return default(T);
                }

                return (T) JsonSerializer.DeserializeFromString(entry.Value, typeof (T));
            }
        }

        public void Set<T>(string key, T value)
        {
            using (var work = _store.Begin())
            {
                var entry = work.Get<CacheEntry>(x => x.Key == key).FirstOrDefault();

                if (value == null)
                {
                    work.Delete<CacheEntry>(key);
                    return;
                }

                bool isNew = false;
                if (entry == null)
                {
                    isNew = true;
                    entry = new CacheEntry() {Key = key};
                }

                entry.DateModified = DateTime.UtcNow;
                entry.Value = JsonSerializer.SerializeToString(value);
                entry.Type = value.GetType().AssemblyQualifiedName;

                if (isNew)
                    work.Add(entry);
                else
                    work.Update(entry);
            }
        }
    }
}
