#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using SQLite;

namespace ZebraFlickr.Core.Store
{
    public class StoreWork : IDisposable
    {
        private readonly Action<SQLiteConnection> _onDisposed;
        private SQLiteConnection _connection;
        private Transaction _transaction;
        private int _usageCount = 1;

        public StoreWork(SQLiteConnection connection, Action<SQLiteConnection> onDisposed)
        {
            _connection = connection;
            _onDisposed = onDisposed;
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            _usageCount--;

            if (_usageCount <= 0)
            {
                Debug.Assert(_connection != null);

                _onDisposed(_connection);
                _connection = null;
            }
        }

        #endregion

        public void IncrementUsages()
        {
            _usageCount++;
        }

        public ITransaction BeginTransaction()
        {
            if (_transaction != null)
            {
                _transaction.IncrementUsages();
                return _transaction;
            }

            _transaction = new Transaction(_connection, () => _transaction = null);
            return _transaction;
        }

        public void Add<T>(T value)
        {
            _connection.Insert(value);
        }

        public void Update<T>(T value)
        {
            _connection.Update(value);
        }

        public T FirstOrDefault<T>() where T : new()
        {
            return _connection.Table<T>().FirstOrDefault();
        }

        public T FirstOrDefault<T>(Expression<Func<T, bool>> @where) where T : new()
        {
            return _connection.Table<T>().Where(where).FirstOrDefault();
        }

        public T GetById<T>(int id) where T : new()
        {
            return _connection.Find<T>(id);
        }

        public IEnumerable<T> Get<T>() where T : new()
        {
            return _connection.Table<T>().Deferred();
        }

        public IEnumerable<T> Get<T>(Expression<Func<T, bool>> @where) where T : new()
        {
            return _connection.Table<T>().Deferred().Where(@where);
        }

        public void Delete<T>(object id)
        {
            _connection.Delete<T>(id);
        }

        public void DeleteAll<T>()
        {
            _connection.DeleteAll<T>();
        }

        public void Execute(string sql, params object[] args)
        {
            _connection.Execute(sql, args);
        }
    }
}