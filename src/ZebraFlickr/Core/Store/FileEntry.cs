#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using SQLite;

namespace ZebraFlickr.Core.Store
{
	public class FileEntry
    {
		[PrimaryKey, AutoIncrement]
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual long Size { get; set; }

        public virtual DateTime Modified { get; set; }

		[Indexed]
        public virtual byte[] Hash { get; set; }

		public virtual bool Uploaded { get; set; }

		public virtual bool UploadedSet { get; set; }

		public virtual  bool UploadedMetaData { get; set; }

		public virtual string PictureId { get; set; }

        public virtual Dictionary<string, string> PicasaValues { get; set; }

        public virtual string[] FlickrTags { get; set; }

        public virtual bool Skip { get; set; }

        public virtual bool ContentChanged { get; set; }

        public virtual bool MetaDataChanged { get; set; }

        public virtual DateTime? Synced { get; set; }

        public virtual byte[] ExifHash { get; set; }
	}
}