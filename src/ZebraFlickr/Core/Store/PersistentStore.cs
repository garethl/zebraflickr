﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using SQLite;
using ZebraFlickr.Core.Store.SQLite;

namespace ZebraFlickr.Core.Store
{
    public class PersistentStore : IDisposable
    {
        private readonly string _file;
        private readonly StoreCache _cache;
        private SQLiteConnection _connection;

        private readonly ConcurrentStack<SQLiteConnection> _connections = new ConcurrentStack<SQLiteConnection>();
        
        [ThreadStatic]
        private static StoreWork _localWork;

        static PersistentStore()
        {
            Loader.Load();

            //EnableSharedCache();
        }
        
		[DllImport("sqlite3", EntryPoint = "sqlite3_enable_shared_cache", CallingConvention = CallingConvention.Cdecl)]
		public static extern int EnableSharedCache(int enable);

        private static void EnableSharedCache()
        {
            var result = EnableSharedCache(1);
        }

        public PersistentStore(string file)
        {
            _file = file;
            _connection = CreateConnection();

            _connection.BeginTransaction();

            try
            {
                _connection.CreateTable<FileEntry>();
                _connection.CreateTable<UserDetails>();
                _connection.CreateTable<SetEntry>();
                _connection.CreateTable<FileSetLink>();
                _connection.CreateTable<CacheEntry>();

                UpgradeOld();

                _connection.Commit();
            }
            catch (Exception)
            {
                _connection.Rollback();
                throw;
            }

            _cache = new StoreCache(this);
        }

        public StoreCache Cache
        {
            get { return _cache; }
        }

        public StoreWork Begin()
        {
            if (_localWork != null)
            {
                _localWork.IncrementUsages();
                return _localWork;
            }

            _localWork = new StoreWork(CreateConnection(),
                                       cn =>
                                           {
                                               _connections.Push(cn);
                                               _localWork = null;
                                           });
            return _localWork;
        }

        private SQLiteConnection CreateConnection()
        {
            SQLiteConnection ret;
            if (_connections.TryPop(out ret))
                return ret;

            var connection = new SQLiteConnection(_file, true);

            connection.Execute("PRAGMA journal_mode = WAL;");
            connection.Execute("PRAGMA cache_size = 10000;");
            connection.Execute("PRAGMA synchronous = NORMAL;");
            connection.Execute("PRAGMA auto_vacuum = INCREMENTAL;");

            return connection;
        }

        #region IDisposable Members

        public void Dispose()
        {
            foreach (var connection in _connections)
                connection.Dispose();

            if (_connection != null)
            {
                _connection.Dispose();
                _connection = null;
            }
        }

        #endregion

        private void UpgradeOld()
        {
            if (_connection.ExecuteScalar<int>("select count(*) from FileSetLink") == 0)
            {
                using (var work = BeginInternal())
                {
                    var sets = work.Get<SetEntry>().ToDictionary(x => x.Name, StringComparer.OrdinalIgnoreCase);

                    foreach (var entry in work.Get<FileEntry>())
                    {
                        if (!entry.UploadedSet)
                            continue;

                        var parts = entry.Name.Split(new[] {'\\', '/'});
                        var setName = string.Join(@"\", parts.Skip(1).Take(parts.Length - 1 - 1));

                        if (string.IsNullOrEmpty(setName))
                            setName = "Unorganised";

                        SetEntry set;
                        if (sets.TryGetValue(setName, out set))
                        {
                            work.Add(new FileSetLink
                                {
                                    FileEntryId = entry.Id,
                                    SetId = set.Id
                                });
                        }
                    }
                }
            }
        }

        private StoreWork BeginInternal()
        {
            return new StoreWork(_connection, (cn) => { });
        }

        public void Compact(bool full = false)
        {
            _connection.Execute(full ? "VACUUM" : "PRAGMA incremental_vacuum(0);");
        }
    }
}