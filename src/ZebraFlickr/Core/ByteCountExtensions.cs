﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;

namespace ZebraFlickr.Core
{
	public static class ByteCountExtensions
	{
		/// <summary>
		/// Gets a formatted number
		/// </summary>
		/// <param name = "value">The raw value</param>
		/// <returns>The formatted value</returns>
		public static string ToFormattedString(this int value)
		{
			return value.ToString("#,0");
		}

		/// <summary>
		/// Gets a formatted number
		/// </summary>
		/// <param name = "value">The raw value</param>
		/// <returns>The formatted value</returns>
		public static string ToFormattedString(this long value)
		{
			return value.ToString("#,0");
		}

		/// <summary>
		/// Gets a formatted byte count string
		/// </summary>
		/// <param name="value">The raw value</param>
		/// <returns>The formatted value</returns>
		public static string ToFormattedByteCountString(this int value)
		{
			return ((double) value).ToFormattedByteCountString();
		}

		/// <summary>
		/// Gets a formatted byte count string
		/// </summary>
		/// <param name="value">The raw value</param>
		/// <param name="decimalPlaces">Decimal places to use</param>
		/// <returns>The formatted value</returns>
		public static string ToFormattedByteCountString(this int value, int decimalPlaces)
		{
			return ((double) value).ToFormattedByteCountString(decimalPlaces);
		}

		/// <summary>
		/// Gets a formatted byte count string
		/// </summary>
		/// <param name="value">The raw value</param>
		/// <returns>The formatted value</returns>
		public static string ToFormattedByteCountString(this long value)
		{
			return ((double) value).ToFormattedByteCountString();
		}

		/// <summary>
		/// Gets a formatted byte count string
		/// </summary>
		/// <param name="value">The raw value</param>
		/// <param name="decimalPlaces">Decimal places to use</param>
		/// <returns>The formatted value</returns>
		public static string ToFormattedByteCountString(this long value, int decimalPlaces)
		{
			return ((double) value).ToFormattedByteCountString(decimalPlaces);
		}

		/// <summary>
		/// Gets a formatted byte count string
		/// </summary>
		/// <param name="value">The raw value</param>
		/// <returns>The formatted value</returns>
		public static string ToFormattedByteCountString(this double value)
		{
			return ToFormattedByteCountString(value, 0);
		}

		/// <summary>
		/// Gets a formatted byte count string
		/// </summary>
		/// <param name="value">The raw value</param>
		/// <param name="decimalPlaces">Decimal places to use</param>
		/// <returns>The formatted value</returns>
		public static string ToFormattedByteCountString(this double value, int decimalPlaces)
		{
			const double kb = 1024;
			const double mb = kb*1024;
			const double gb = mb*1024;
			const double tb = gb*1024;

			string formatString = "#,0";

			if (decimalPlaces > 0)
				formatString += "." + new string('0', decimalPlaces);

			string suffix;
			double v;

			if (value < kb)
			{
				suffix = " B";
				v = value;
			}
			else if (value < mb)
			{
				suffix = " KB";
				v = (value/kb);
			}
			else if (value < gb)
			{
				suffix = " MB";
				v = (value/mb);
			}
			else if (value < tb)
			{
				suffix = " GB";
				v = (value/gb);
			}
			else
			{
				suffix = " TB";
				v = (value/tb);
			}

			v = Math.Round(v, decimalPlaces);

			return v.ToString(formatString) + suffix;
		}
	}
}