#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;

namespace ZebraFlickr
{
	public static class Log
	{
		private static LogLevel _level = LogLevel.Debug;
		private static ILogger _logger;

		static Log()
		{
			_logger = new CompositeLogger(new ConsoleLogger(), new FileLogger("zebraflickr.log"));
		}

		public static void Configure(ILogger logger)
		{
			_logger = logger;
		}

		public static void SetLevel(LogLevel level)
		{
			_level = level;
		}

		public static void Debug(string message, params object[] args)
		{
			DoLog(LogLevel.Debug, message, null, args);
		}

		public static void Debug(string message, Exception ex, params object[] args)
		{
			DoLog(LogLevel.Debug, message, ex, args);
		}

		public static void Info(string message, params object[] args)
		{
			DoLog(LogLevel.Info, message, null, args);
		}

		public static void Info(string message, Exception ex, params object[] args)
		{
			DoLog(LogLevel.Info, message, ex, args);
		}

		public static void Warn(string message, params object[] args)
		{
			DoLog(LogLevel.Warn, message, null, args);
		}

		public static void Warn(string message, Exception ex, params object[] args)
		{
			DoLog(LogLevel.Warn, message, ex, args);
		}


		public static void Error(string message, params object[] args)
		{
			DoLog(LogLevel.Error, message, null, args);
		}

		public static void Error(string message, Exception ex, params object[] args)
		{
			DoLog(LogLevel.Error, message, ex, args);
		}

		public static void Fatal(string message, params object[] args)
		{
			DoLog(LogLevel.Fatal, message, null, args);
		}

		public static void Fatal(string message, Exception ex, params object[] args)
		{
			DoLog(LogLevel.Fatal, message, ex, args);
		}

		private static void DoLog(LogLevel level, string message, Exception ex, object[] args)
		{
			if (level < _level)
				return;

			_logger.WriteLog(DateTime.Now, level, args.Length > 0 ? string.Format(message, args) : message, ex);
		}

		public static string SerializeException(Exception exception)
		{
			return Environment.NewLine + exception;
		}
	}
}