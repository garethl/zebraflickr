﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Diagnostics;
using System.Text;
using ZebraFlickr.CommandLine;
using ZebraFlickr.CommandLine.Exceptions;

namespace ZebraFlickr
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var startColor = Console.ForegroundColor;

		    Console.OutputEncoding = Encoding.UTF8;

			Console.CursorVisible = false;

			Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
				{
					if (CancelHandler.Cancelled)
					{
						Log.Warn("Aborting all processes NOW");
						Console.ForegroundColor = startColor;
						Environment.Exit(1);
					}
					else
					{
						Log.Warn("Cancelling process - attempting to do it gracefully. Pressing Ctrl+C again will exit immedietly.");
						e.Cancel = true;
						CancelHandler.Cancel();
					}
				};

			try
			{
				IAction action = null;
				try
				{
					action = ActionHelper.Resolve(args);

					if (action == null)
						throw new CommandLineException("Unrecognised command line");

					action.Run();
				}
				catch (ValidationException ex)
				{
					var writer = Console.Error;

					BaseAction.PrintLogo(false, writer);

					writer.WriteLine("Error: {0}", ex.Message);
					writer.WriteLine("try 'zebraflickr help' for more information.");
					Environment.ExitCode = 1;
				}
				catch (Exception ex)
				{
					var writer = Console.Error;

					BaseAction.PrintLogo(false, writer);

					writer.WriteLine("Error: " + ex.Message);
					writer.WriteLine();

					//if the action didn't even get made, or the verbosity is debug, dump the stack trace out
					if (action == null || action.Verbosity >= LogLevel.Debug)
					{
						writer.WriteLine("Full error: " + ex);
					}

					Environment.ExitCode = 1;
				}
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(ex.ToString());
				Environment.ExitCode = 1;
			}
			finally
			{
				Console.ForegroundColor = startColor;

				if (Debugger.IsAttached)
					Console.ReadLine();
			}
		}
	}
}