﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Globalization;
using ZebraFlickr.Core;
using ZebraFlickr.Core.Processing;

namespace ZebraFlickr.Actions
{
    public class SyncAction : UploadAction
    {
        public SyncAction()
            : base("sync", "Uploads new photos, and downloads any photos not stored locally.")
        {
        }

        #region Overrides of BaseAction

        /// <summary>
        /// Run the action
        /// </summary>
        public override void Run()
        {
            var config = GetConfiguration();

            Log.Info("Syncing local folder {0} with Flickr", config.RootDirectory);

            LogConfiguration();

            using (var state = new LocalState(config))
            {
                Log.Info("-----------------------------------------------------");
                Log.Info("Scanning local files...");

                LoadLocalFiles(config, state);

                if (!CancelHandler.Cancelled)
                {
                    Log.Info("-----------------------------------------------------");
                    Log.Info("Downloading information from flickr...");
                    RetrieveFlickrInfo(state);
                }

                Log.Info("-----------------------------------------------------");
                Log.Info("Uploading local differences");

                DoUpload(state, config);

                if (!CancelHandler.Cancelled)
                    state.Compact();
            }

            Log.Info("-----------------------------------------------------");

            Log.Info("Finished");
        }

        private void RetrieveFlickrInfo(LocalState state)
        {
            var details = CredentialHandler.GetUserDetails(state);

            var retriever = new FlickrRetriever(state, details, Path);
            retriever.Retrieve(CancelHandler.Token);
        }

        private void LoadLocalFiles(Config config, LocalState state)
        {
            var source = new FileSource(config.RootDirectory);

            Pipeline
                .Source(source)
                .Process(new EntryProcessor(state, Path).Execute)
                .Consume(x => { })
                .Wait(CancelHandler.Token);

            Log.Info("Found {0} files", state.Lookup.FilesByName.Count);
        }

        #endregion
    }
}