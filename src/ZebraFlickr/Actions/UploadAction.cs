﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System.IO;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using ZebraFlickr.CommandLine;
using ZebraFlickr.CommandLine.Exceptions;
using ZebraFlickr.Core;
using ZebraFlickr.Core.Processing;

namespace ZebraFlickr.Actions
{
    public class UploadAction : BaseConfiguredAction
    {
        public UploadAction()
            : base("upload", "Uploads all new and changed files to flickr.")
        {
        }

        public UploadAction(string name, string description = null, string longDescription = null)
            : base(name, description, longDescription)
        {
        }

        [Details(Description = "Root path of your photos", LongName = "path", ShortName = "p", Required = true)]
        public string Path { get; set; }

        [Details(Description = "Subfolder to operate on, within the path", ShortName = "s", LongName = "subfolder")]
        public string SubFolder { get; set; }

        [Details(Description = "Upload photos with privacy set to public", Default = false)]
        public bool Public { get; set; }

        [Details(Description = "Upload photos with privacy set to family", Default = false)]
        public bool Family { get; set; }

        [Details(Description = "Upload photos with privacy set to friend", Default = false)]
        public bool Friend { get; set; }

        [Details(Description = "Ignores top level directories matching the number given. Useful if you store your sets in annual folders, for example.", LongName = "levelignore",
            Default = 0)]
        public int DirectoryLevelsToIgnore { get; set; }

        #region Overrides of BaseAction

        protected override void Validate()
        {
            base.Validate();

            if (!Directory.Exists(Path))
                throw new ValidationException(string.Format("Path directory {0} does not exist", Path));

            if (!string.IsNullOrEmpty(SubFolder) && !Directory.Exists(System.IO.Path.Combine(Path, SubFolder)))
                throw new ValidationException(string.Format("Sub folder {0} does not exist", SubFolder));
        }

        /// <summary>
        /// Run the action
        /// </summary>
        public override void Run()
        {
            var config = GetConfiguration();

            Log.Info("Starting Flickr upload of files in {0}", config.RootDirectory);

            LogConfiguration();

            using (var state = new LocalState(config))
            {
                DoUpload(state, config);

                if (!CancelHandler.Cancelled)
                    state.Compact();

                Log.Info("Finished");
            }
        }

        protected void DoUpload(LocalState state, Config config)
        {
            var details = CredentialHandler.GetUserDetails(state);

            var source = new FileSource(config.RootDirectory);

            Pipeline

                //the source files
                .Source(source)

                //load / create entries
                .Process(new EntryProcessor(state, Path).Execute, new ExecutionDataflowBlockOptions {MaxDegreeOfParallelism = 1})

                //upload the file to flickr
                .Process(new FlickrUploader(Path, state, details).Execute, new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 1 })

                //set the set
                .Process(new FlickrSetAssociator(state, details).Execute, new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 1 })

                //set the meta data
                .Process(new FlickrMetaDataSetter(state, details).Execute, new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 1 })

                //dump it
                .Consume(new LogEntryWriter().Handle, new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 1 })

                //wait for it to finish
                .Wait(CancelHandler.Token)
                ;
        }

        protected void LogConfiguration()
        {
            Log.Info("Configuration: ");
            Log.Info("\t path = {0}", Path);
            Log.Info("\t subfolder = {0}", SubFolder);
            Log.Info("\t levelignore = {0}", DirectoryLevelsToIgnore);
            Log.Info("\t family = {0}", Family);
            Log.Info("\t friend = {0}", Friend);
            Log.Info("\t public = {0}", Public);
        }

        protected Config GetConfiguration()
        {
            var root = string.IsNullOrEmpty(SubFolder) ? Path : System.IO.Path.Combine(Path, SubFolder);

            var config = new Config
                {
                    DirectoryLevelsToIgnore = DirectoryLevelsToIgnore,
                    Family = Family,
                    Friend = Friend,
                    Public = Public,
                    RootDirectory = root
                };

            Path = System.IO.Path.GetFullPath(Path);
            return config;
        }

        #endregion
    }
}