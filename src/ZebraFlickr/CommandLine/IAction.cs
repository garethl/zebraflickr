#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System.IO;

namespace ZebraFlickr.CommandLine
{
	/// <summary>
	/// An action
	/// </summary>
	public interface IAction
	{
		/// <summary>
		/// Name of the action
		/// </summary>
		string ActionName { get; }

		/// <summary>
		/// Is this the default action?
		/// </summary>
		bool IsDefault { get; }

		/// <summary>
		/// Verbosity level
		/// </summary>
		LogLevel Verbosity { get; set; }

		/// <summary>
		/// Get the description
		/// </summary>
		/// <returns></returns>
		string Description { get; }

		/// <summary>
		/// Get the long description
		/// </summary>
		/// <returns></returns>
		string LongDescription { get; }

		/// <summary>
		/// Run the action
		/// </summary>
		void Run();

		/// <summary>
		/// Initializes the action prior to running it
		/// </summary>
		/// <param name="arguments">Arguments</param>
		void Initialize(string[] arguments);

		/// <summary>
		/// The action should print its help string to the text writer
		/// </summary>
		/// <param name="writer">Text writer to use</param>
		void PrintHelpString(TextWriter writer);

		/// <summary>
		/// The action should return its quick help string
		/// </summary>
		string GetUsageDescription();

		/// <summary>
		/// Try and cleanly abort the action
		/// </summary>
		void Abort();
	}
}