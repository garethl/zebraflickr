#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Reflection;

namespace ZebraFlickr.CommandLine
{
	/// <summary>
	/// Argument map
	/// </summary>
	public class ArgumentMap : DetailsAttribute
	{
		/// <summary>
		/// The decoder
		/// </summary>
		private IArgumentDecoder _decoderInst;

		/// <summary>
		/// The property
		/// </summary>
		public PropertyInfo Property { get; set; }

		/// <summary>
		/// Checks if the decoder found a value
		/// </summary>
		public bool HasValue
		{
			get { return GetDecoder().HasValue; }
		}

		/// <summary>
		/// Decoder to use
		/// </summary>
		public override Type Decoder
		{
			get { return base.Decoder; }
			set
			{
				base.Decoder = value;
				_decoderInst = null;
			}
		}

		/// <summary>
		/// Gets the decoder instance
		/// </summary>
		public IArgumentDecoder GetDecoder()
		{
			if (_decoderInst != null)
				return _decoderInst;

			IArgumentDecoder decoder;

			if (Decoder != null)
			{
				decoder = (IArgumentDecoder) Activator.CreateInstance(Decoder);
			}
			else if (Property.PropertyType.IsEnum)
			{
				decoder = new EnumArgumentDecoder();
			}
			else if (Property.PropertyType == typeof (TimeSpan))
			{
				decoder = new TimeSpanArgumentDecoder();
			}
			else
			{
				decoder = new DefaultArgumentDecoder();
			}

			decoder.Initialize(this);

			_decoderInst = decoder;
			return decoder;
		}

		/// <summary>
		/// Sets the value from the argument
		/// </summary>
		/// <param name="item">Item to set the argument on</param>
		/// <param name="argument">The argument</param>
		public void SetValue(object item, Parser.Argument argument)
		{
			var decoder = GetDecoder();
			decoder.Decode(item, argument);
		}
	}
}