#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using ZebraFlickr.CommandLine.Exceptions;

namespace ZebraFlickr.CommandLine
{
	/// <summary>
	/// Default argument decoder
	/// </summary>
	public class DefaultArgumentDecoder : IArgumentDecoder
	{
		private ArgumentMap _map;

		#region IArgumentDecoder Members

		/// <summary>
		/// Decode an argument
		/// </summary>
		/// <param name="item">Item to set the argument on</param>
		/// <param name="argument">The argument</param>
		public virtual void Decode(object item, Parser.Argument argument)
		{
			Type type = _map.Property.PropertyType;
			if (type == typeof (string))
			{
				_map.Property.SetValue(item, argument.Value, null);
			}
			else if (type == typeof (bool))
			{
				_map.Property.SetValue(item, true, null);
			}
			else if (type == typeof (int))
			{
				int num;
				if (!int.TryParse(argument.Value, out num))
				{
					throw new ValidationException(string.Format("Unable to parse argument {0}", argument.Key));
				}
				_map.Property.SetValue(item, num, null);
			}
			else if (type == typeof (DateTime))
			{
				DateTime dateTime;
				if (DateTime.TryParse(argument.Value, out dateTime))
				{
					_map.Property.SetValue(item, dateTime, null);
				}
				else
				{
					throw new ValidationException(string.Format("Unable to parse argument {0}", argument.Key));
				}
			}
			else
			{
				throw new ValidationException(string.Format("Unable to parse argument {0}", argument.Key));
			}
		}

		/// <summary>
		/// Initialize the decoder with the map
		/// </summary>
		/// <param name="map">The argument map</param>
		public void Initialize(ArgumentMap map)
		{
			_map = map;
		}

		/// <summary>
		/// Did the decoder find a value?
		/// </summary>
		public bool HasValue
		{
			get
			{
				return ((_map.Property.PropertyType != typeof (bool)) &&
				        (_map.Property.PropertyType != typeof (IList<bool>)));
			}
		}

		#endregion
	}
}