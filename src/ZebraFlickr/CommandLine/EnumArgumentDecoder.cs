#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace ZebraFlickr.CommandLine
{
	public class EnumArgumentDecoder : IArgumentDecoder
	{
		private ArgumentMap _map;

		#region Implementation of IArgumentDecoder

		/// <summary>
		/// Decode an argument
		/// </summary>
		/// <param name="item">Item to set the argument on</param>
		/// <param name="argument">The argument</param>
		public void Decode(object item, Parser.Argument argument)
		{
			var enumType = _map.Property.PropertyType;
			var enumValue = Enum.Parse(enumType, argument.Value, true);

			_map.Property.SetValue(item, enumValue, null);
		}

		/// <summary>
		/// Initialize the decoder with the map
		/// </summary>
		/// <param name="map">The argument map</param>
		public void Initialize(ArgumentMap map)
		{
			_map = map;

			var enumType = _map.Property.PropertyType;

			var names = GetNames(enumType);


			var defaultDescription = "";

			if (map.Default != null)
			{
				var values = Enum.GetValues(enumType);

				for (int x = 0; x < values.Length; x++)
				{
					if (values.GetValue(x).Equals(map.Default))
					{
						defaultDescription = names[x];
						break;
					}
				}

				if (!string.IsNullOrEmpty(defaultDescription))
				{
					defaultDescription = ". Default = " + defaultDescription;
				}
			}

			var descriptionAddition = string.Format(" ({0}){1}", FancyJoin(", ", " or ", names), defaultDescription);

			_map.Description += descriptionAddition;
		}

		/// <summary>
		/// Did the decoder find a value?
		/// </summary>
		public bool HasValue
		{
			get { return true; }
		}

		private static string FancyJoin(string separator, string lastSeparator, string[] values)
		{
			if (values.Length == 0)
				return string.Empty;

			if (values.Length == 1)
				return values[0];


			var sb = new StringBuilder();

			for (int x = 0; x < values.Length; x++)
			{
				if (x > 0)
				{
					if (x < values.Length - 1)
						sb.Append(separator);
					else
						sb.Append(lastSeparator);
				}

				sb.Append(values[x]);
			}

			return sb.ToString();
		}

		private static string[] GetNames(Type type)
		{
			var ret = new List<string>();

			var fields = type.GetFields(BindingFlags.Public | BindingFlags.Static);

			foreach (var field in fields)
			{
				var attribs = field.GetCustomAttributes(typeof (DescriptionAttribute), true);

				string name;

				if (attribs.Length > 0)
				{
					name = ((DescriptionAttribute) attribs[0]).Description;
				}
				else
				{
					name = field.Name;
				}

				ret.Add(name);
			}

			return ret.ToArray();
		}

		#endregion
	}
}