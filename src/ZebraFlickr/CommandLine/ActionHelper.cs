﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ZebraFlickr.CommandLine
{
	public static class ActionHelper
	{
		private static Dictionary<string, IAction> _actions;
		private static IAction _defaultAction;

		static ActionHelper()
		{
			Initialize();
		}

		/// <summary>
		/// All the actions
		/// </summary>
		public static IEnumerable<IAction> All
		{
			get { return _actions.Values; }
		}

		/// <summary>
		/// The default action
		/// </summary>
		public static IAction Default
		{
			get { return _defaultAction; }
		}

		/// <summary>
		/// Gets a specific action
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static IAction Get(string name)
		{
			IAction ret;
			if (_actions.TryGetValue(name, out ret))
				return ret;

			return null;
		}

		/// <summary>
		/// Initialize the helper
		/// </summary>
		private static void Initialize()
		{
			_actions = LoadActions(Assembly.GetExecutingAssembly());
		}

		/// <summary>
		/// Load the actions
		/// </summary>
		/// <param name="assembly">Assembly to load from</param>
		/// <returns>A dictionary containing the actions</returns>
		private static Dictionary<string, IAction> LoadActions(Assembly assembly)
		{
			var actionList = assembly.GetTypes().Where(t => typeof (IAction).IsAssignableFrom(t) && !t.IsAbstract);

			var actions = new Dictionary<string, IAction>(StringComparer.InvariantCultureIgnoreCase);

			foreach (var action in actionList)
			{
				var instance = (IAction) Activator.CreateInstance(action);
				actions[instance.ActionName] = instance;

				if (instance.IsDefault)
					_defaultAction = instance;
			}
			return actions;
		}

		/// <summary>
		/// Resolves the arguments to an action
		/// </summary>
		/// <param name="args">Command line args</param>
		public static IAction Resolve(string[] args)
		{
			IAction action = null;
			if (args.Length > 0)
			{
				action = Get(args[0]);
			}

			if (action == null)
				action = Default;

			if (action == null)
				return null;

			action.Initialize(args);
			return action;
		}
	}
}