﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZebraFlickr.CommandLine
{
	public static class HelpBuilder
	{
		private const int DescriptionPaddingLeft = 28;

		public static int ConsoleWidth
		{
			get
			{
				try
				{
					return Math.Min(Console.WindowWidth, 80);
				}
				catch
				{
					return 80;
				}
			}
		}

		public static string GetUsage(ArgumentMap[] map, Func<ArgumentMap, bool> filter)
		{
			var sb = new StringBuilder();

			foreach (var item in map.Where(m => !m.IsBareOption).Where(filter))
			{
				var args = BuildArgString(item);
				var description = BuildDescriptionString(item);

				description = description.Reflow(ConsoleWidth - DescriptionPaddingLeft - 5);

				var output = AddMultiLineStrings(args, description, DescriptionPaddingLeft, " : ");

				sb.Append(output);
				sb.AppendLine();
			}

			return sb.ToString();
		}

		public static string GetBareUsage(ArgumentMap[] map)
		{
			var ret = new List<string>();

			foreach (var item in map.Where(m => m.IsBareOption).OrderBy(m => m.Index))
			{
				ret.Add(BuildBareArgString(item));
			}

			return string.Join(" ", ret.ToArray());
		}

		public static string GetBareDetailedUsage(ArgumentMap[] map)
		{
			var sb = new StringBuilder();

			foreach (var item in map.Where(m => m.IsBareOption).OrderBy(m => m.Index))
			{
				var arg = BuildBareArgString(item);
				string description = BuildDescriptionString(item);

				description = description.Reflow(ConsoleWidth - DescriptionPaddingLeft - 5);

				var output = AddMultiLineStrings(arg, description, DescriptionPaddingLeft, " : ");

				sb.Append(output);
				sb.AppendLine();
			}

			return sb.ToString();
		}

		private static string BuildBareArgString(ArgumentMap item)
		{
			return string.Format("<{0}>", item.LongName);
		}

		private static string AddMultiLineStrings(string a, string b, int left, string separator)
		{
			var separatorLength = separator.Length;

			var aLines = a.Trim()
				.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries)
				.Select(s => "  " + s)
				.GetEnumerator();

			var bLines = b.Trim()
				.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries)
				.Cast<string>()
				.GetEnumerator();

			bool isFirstLine = true;

			var sb = new StringBuilder(a.Length + b.Length);

			while (true)
			{
				bool stillHaveA = aLines.MoveNext();
				bool stillHaveB = bLines.MoveNext();

				if (stillHaveA)
				{
					sb.Append(aLines.Current.PadRight(left));

					if (isFirstLine && stillHaveB)
					{
						sb.Append(separator);
						isFirstLine = false;
					}
					else if (stillHaveB)
					{
						sb.Append(new string(' ', separatorLength));
					}
				}
				else if (stillHaveB) //no a, but b
				{
					sb.Append(new string(' ', left + separatorLength));
				}

				if (stillHaveB)
				{
					sb.Append(bLines.Current);
				}

				if (!stillHaveA && !stillHaveB)
					break;

				sb.AppendLine();
			}

			return sb.ToString().TrimEnd();
		}

		private static string BuildDescriptionString(ArgumentMap item)
		{
			var ret = item.Description;

			if (item.Values != null && item.Values.Length > 0)
			{
				ret += string.Format("{0}{1}  Values: {2})",
				                     Environment.NewLine,
				                     new string(' ', DescriptionPaddingLeft),
				                     string.Join(", ", item.Values));
			}

			return ret;
		}

		private static string FixNewLines(string ret, int leftPadding)
		{
			if (ret.Contains(Environment.NewLine))
			{
				ret = ret.Replace(Environment.NewLine, Environment.NewLine + new string(' ', leftPadding));
			}
			return ret;
		}

		private static string BuildArgString(ArgumentMap item)
		{
			const string shortPrefix = "-";
			const string longPrefix = "--";

			IEnumerable<string> shortArgs = GetArgs(shortPrefix, item.ShortName, item.OtherShortNames, item, false);
			IEnumerable<string> longArgs = GetArgs(longPrefix, item.LongName, item.OtherLongNames, item, true);

			var currentLineLength = 0;
			var isFirst = true;
			var sb = new StringBuilder();

			if (shortArgs.Any() || longArgs.Any())
				sb.Append("  ");

			foreach (var shortArg in shortArgs.Concat(longArgs))
			{
				if (!isFirst)
				{
					sb.Append(", ");
					currentLineLength += 2;
				}
				else
				{
					isFirst = false;
				}

				if (currentLineLength + shortArg.Length > DescriptionPaddingLeft)
				{
					sb.AppendLine();
					currentLineLength = 4;
					sb.Append("    ");
				}

				sb.Append(shortArg);
				currentLineLength += shortArg.Length;
			}

			return sb.ToString();
		}

		private static IEnumerable<string> GetArgs(string prefix,
		                                           string mainArg,
		                                           IEnumerable<string> secondaryArgs,
		                                           ArgumentMap item,
		                                           bool isLong)
		{
			mainArg = string.IsNullOrEmpty(mainArg)
				          ? null
				          : prefix + mainArg + (isLong && item.HasValue ? "=VALUE" : null);

			var otherShortArgs = secondaryArgs == null
				                     ? new string[] {}
				                     : secondaryArgs.Select(
					                     i => prefix + i + (isLong && item.HasValue ? "=VALUE" : null));

			return mainArg == null ? otherShortArgs : new[] {mainArg}.Concat(otherShortArgs);
		}
	}
}