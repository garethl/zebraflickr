﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ZebraFlickr.CommandLine
{
	public static class StringExtensions
	{
		/// <summary>
		/// Reflows text to fit into a specified character width
		/// </summary>
		/// <param name="value">The text to reflow</param>
		/// <param name="maxWidth">The maximum width of each line (in characters)</param>
		/// <returns>The reflowed string</returns>
		public static string Reflow(this string value, int maxWidth)
		{
			return value.Reflow(maxWidth, null, null);
		}

		/// <summary>
		/// Reflows text to fit into a specified character width
		/// </summary>
		/// <param name="value">The text to reflow</param>
		/// <param name="maxWidth">The maximum width of each line (in characters)</param>
		/// <param name="prefix">Prefix for each line</param>
		/// <param name="postfix">Postfix for each line</param>
		/// <returns>The reflowed string</returns>
		public static string Reflow(this string value, int maxWidth, string prefix, string postfix)
		{
			if (value == null)
				value = "";

			if (value.Length < maxWidth)
				return value;

			//handle text with multiple lines and potentially paragraphs
			if (value.IndexOf(Environment.NewLine) >= 0)
			{
				string[] paragraphs = Regex.Split(value, @"^\s*$", RegexOptions.Multiline | RegexOptions.Compiled);

				var sb = new StringBuilder();

				int index = 0;

				foreach (var paragraph in paragraphs)
				{
					if (index > 0)
					{
						sb.AppendLine();
						sb.AppendLine();
					}

					var p = paragraph;

					if (p.Contains(Environment.NewLine))
						p = string.Join(" ",
						                p.Split(new[] {Environment.NewLine}, StringSplitOptions.None).Select(
							                x => x.Trim()));

					sb.Append(ReflowSingleLine(p, maxWidth, prefix, postfix));

					index++;
				}

				return sb.ToString();
			}
			else
			{
				return ReflowSingleLine(value, maxWidth, prefix, postfix);
			}
		}

		private static string ReflowSingleLine(string value, int maxWidth, string prefix, string postfix)
		{
			var sb = new StringBuilder();

			int start = 0;
			int endPos = maxWidth;

			while (endPos < value.Length)
			{
				bool successful = false;

				//find white space before the ideal break
				for (int i = endPos; i > start; i--)
				{
					if (char.IsWhiteSpace(value[i]))
					{
						if (!string.IsNullOrEmpty(prefix))
							sb.Append(prefix);
						sb.Append(value.Substring(start, i - start).Trim());
						if (!string.IsNullOrEmpty(postfix))
							sb.Append(postfix);
						sb.AppendLine();
						start = i;
						endPos = i + maxWidth;
						successful = true;
						break;
					}
				}

				if (!successful)
				{
					if (!string.IsNullOrEmpty(prefix))
						sb.Append(prefix);
					sb.Append(value.Substring(start, endPos - 1).Trim());
					if (!string.IsNullOrEmpty(postfix))
						sb.Append(postfix);
					start = endPos;
					endPos += maxWidth;
				}
			}

			if (start < value.Length)
			{
				if (!string.IsNullOrEmpty(prefix))
					sb.Append(prefix);
				sb.Append(value.Substring(start, value.Length - start).Trim());
				if (!string.IsNullOrEmpty(postfix))
					sb.Append(postfix);
			}

			return sb.ToString();
		}

		public static string Indent(this string value, bool indentFirstLine, int indent)
		{
			if (indentFirstLine)
				return new string(' ', indent) +
				       value.Replace(Environment.NewLine, Environment.NewLine + new string(' ', indent));

			return value.Replace(Environment.NewLine, Environment.NewLine + new string(' ', indent));
		}
	}
}