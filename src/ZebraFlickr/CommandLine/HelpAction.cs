﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Linq;

namespace ZebraFlickr.CommandLine
{
	public class HelpAction : BaseAction
	{
		/// <summary>
		/// Creates a new action
		/// </summary>
		public HelpAction() : base("help")
		{
		}

		/// <summary>
		/// Is this the default action?
		/// </summary>
		public override bool IsDefault
		{
			get { return true; }
		}

		public override string GetUsageDescription()
		{
			return ActionName;
		}

		/// <summary>
		/// Run the action
		/// </summary>
		public override void Run()
		{
			var writer = Console.Out;

			if (string.IsNullOrEmpty(Command)) // no command, list commands
			{
				writer.WriteLine("Usage: zebraflickr <command> [<options>...]");
				writer.WriteLine("Type 'zebraflickr help <command>' for help on a specific command.");

				writer.WriteLine();
				writer.WriteLine("Available commands:");
				foreach (var action in ActionHelper.All.OrderBy(a => a.ActionName))
				{
					if (action == this)
						continue;

					var v = string.Format("    {0,-15}{1}",
					                      action.ActionName,
					                      action.Description.Reflow(HelpBuilder.ConsoleWidth - 20 - 2).Indent(
						                      false, 20));

					writer.WriteLine(v);
				}
			}
			else
			{
				var action = ActionHelper.Get(Command);

				if (action == null)
				{
					writer.WriteLine("Unknown command '{0}'", Command);
				}
				else
				{
					action.PrintHelpString(writer);

					//get all general options
					writer.WriteLine("Global options:");
					var usage = Parser.GetUsage(ActionHelper.All.First().GetType(), x => x.IsGeneral);
					writer.WriteLine(usage);
				}
			}

			Environment.ExitCode = 1;
		}

		#region Arguments

		[Details(IsBareOption = true)]
		public string Command { get; set; }

		#endregion
	}
}