#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.IO;
using System.Reflection;

namespace ZebraFlickr.CommandLine
{
	/// <summary>
	/// Base action
	/// </summary>
	public abstract class BaseAction : IAction
	{
		private static bool _printedLogo;

		/// <summary>
		/// Creates a new action
		/// </summary>
		/// <param name="name">Name of the action</param>
		protected BaseAction(string name, string description = null, string longDescription = null)
		{
			ActionName = name;
			Description = description;
			LongDescription = longDescription;

			Verbosity = LogLevel.Info;
		}

		[Details(Description = "Hide the product name and version number", IsGeneral = true)]
		public virtual bool NoLogo { get; set; }

		/// <summary>
		/// Arguments passed to the action
		/// </summary>
		protected string[] Arguments { get; set; }

		/// <summary>
		/// Is the action aborting
		/// </summary>
		protected bool Aborting { get; set; }

		[Details(Description = "Run in non-interactive mode (any questions required are not acted on)",
			LongName = "non-interactive", Default = false, IsGeneral = true)]
		public bool NonInteractive { get; set; }

		#region IAction Members

		[Details(Description = "Verbosity level", LongName = "verbosity", Default = LogLevel.Info, IsGeneral = true)]
		public virtual LogLevel Verbosity { get; set; }

		/// <summary>
		/// Run the action
		/// </summary>
		public abstract void Run();

		/// <summary>
		/// Initializes the action prior to running it
		/// </summary>
		/// <param name="arguments">Arguments</param>
		public void Initialize(string[] arguments)
		{
			Arguments = arguments;

			if (arguments != null)
			{
				Parser.Parse(this, arguments);
			}

			Log.SetLevel(Verbosity);

			Validate();

			InitializeAction();

			if (!NoLogo)
				PrintLogo(Verbosity <= LogLevel.Debug, Console.Error);
		}

		/// <summary>
		/// The action should print its help string to the text writer
		/// </summary>
		/// <param name="writer">Text writer to use</param>
		public void PrintHelpString(TextWriter writer)
		{
			//usage
			writer.WriteLine("{0}: {1}",
			                 ActionName,
			                 Description.Reflow(HelpBuilder.ConsoleWidth - ActionName.Length - 2 - 2).Indent(
				                 false, ActionName.Length + 2));
			writer.WriteLine("usage: " + GetUsageDescription());

			//long description
			var longDesc = LongDescription;
			if (longDesc != null)
			{
				longDesc = longDesc.Trim(Environment.NewLine.ToCharArray());
				longDesc = longDesc.Reflow(HelpBuilder.ConsoleWidth - 4, "  ", null);
				writer.WriteLine();
				writer.WriteLine(longDesc);
			}

			writer.WriteLine();
			var bare = Parser.GetBareDetailedUsage(GetType());
			if (!string.IsNullOrEmpty(bare))
			{
				writer.WriteLine("Arguments:");
				writer.WriteLine(bare);
			}

			writer.WriteLine("Options:");

			//args
			var usage = Parser.GetUsage(GetType());
			writer.WriteLine(usage);
		}

		/// <summary>
		/// The action should return its quick help string
		/// </summary>
		public virtual string GetUsageDescription()
		{
			return string.Format("{0,-10}[options] {1}", ActionName, Parser.GetBareUsage(GetType()));
		}

		/// <summary>
		/// Try and cleanly abort the action
		/// </summary>
		public virtual void Abort()
		{
			Aborting = true;
		}

		public string ActionName { get; protected set; }

		/// <summary>
		/// Is this the default action?
		/// </summary>
		public virtual bool IsDefault
		{
			get { return false; }
		}

		/// <summary>
		/// Get the description
		/// </summary>
		/// <returns></returns>
		public string Description { get; protected set; }

		/// <summary>
		/// Get the long description
		/// </summary>
		/// <returns></returns>
		public string LongDescription { get; protected set; }

		#endregion

		/// <summary>
		/// Print the logo (app name, version etc)
		/// </summary>
		public static void PrintLogo(bool verbose, TextWriter writer)
		{
			if (_printedLogo)
				return;

			try
			{
				var assembly = Assembly.GetExecutingAssembly();
				var version = assembly.GetName().Version.ToString(3);
				var copyright =
					((AssemblyCopyrightAttribute)
					 assembly.GetCustomAttributes(typeof (AssemblyCopyrightAttribute), true)[0]).Copyright;

				writer.WriteLine("ZebraFlickr, {0}. {1}.", version, copyright);

				if (verbose)
				{
					var description =
						((AssemblyDescriptionAttribute)
						 Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyDescriptionAttribute), true)
							 [0]).Description;

					writer.WriteLine(description);
				}
				writer.WriteLine();
			}
			catch (Exception)
			{
				//ignore
			}

			_printedLogo = true;
		}

		/// <summary>
		/// Validate the action
		/// </summary>
		protected virtual void Validate()
		{
			//
		}

		/// <summary>
		/// Initialize the action
		/// </summary>
		protected virtual void InitializeAction()
		{
			//	
		}
	}
}