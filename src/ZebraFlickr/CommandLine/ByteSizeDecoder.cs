﻿#region License

// Copyright (c) 2012 Gareth Lennox (garethl@dwakn.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of Gareth Lennox nor the names of its
//       contributors may be used to endorse or promote products derived from this
//       software without specific prior written permission.
//  
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

using System;
using System.Collections.Generic;
using ZebraFlickr.CommandLine.Exceptions;

namespace ZebraFlickr.CommandLine
{
	public class ByteSizeDecoder : IArgumentDecoder
	{
		private readonly IDictionary<string, long> _multipliers =
			new Dictionary<string, long>(StringComparer.InvariantCultureIgnoreCase)
				{
					{"k", 1024L},
					{"kb", 1024L},
					{"m", 1024L*1024},
					{"mb", 1024L*1024},
					{"g", 1024L*1024*1024},
					{"gb", 1024L*1024*1024},
					{"t", 1024L*1024*1024*1024},
					{"tb", 1024L*1024*1024*1024},
				};

		private ArgumentMap _map;

		#region IArgumentDecoder Members

		public void Decode(object item, Parser.Argument argument)
		{
			var value = argument.Value;

			var byteValue = Parse(value);

			if (_map.Property.PropertyType == typeof (int))
			{
				if (byteValue > int.MaxValue)
				{
					throw new ValidationException("Value cannot be bigger than 2GB");
				}
				else
				{
					_map.Property.SetValue(item, (int) byteValue, null);
				}
			}
			else
			{
				_map.Property.SetValue(item, byteValue, null);
			}
		}

		public void Initialize(ArgumentMap map)
		{
			_map = map;
		}

		public bool HasValue
		{
			get { return true; }
		}

		#endregion

		private long Parse(string value)
		{
			var digits = new List<char>();
			var letters = new List<char>();

			var isDigits = true;
			foreach (var c in value)
			{
				if (isDigits && char.IsDigit(c))
				{
					digits.Add(c);
				}
				else
				{
					isDigits = false;
					letters.Add(c);
				}
			}

			if (digits.Count == 0)
				throw new ValidationException("Missing number");

			var number = long.Parse(new string(digits.ToArray()));
			var suffix = new string(letters.ToArray());

			if (suffix.Length > 0)
			{
				long multiplier;
				if (_multipliers.TryGetValue(suffix, out multiplier))
				{
					number *= multiplier;
				}
				else
				{
					throw new ValidationException(string.Format("Unknown suffix '{0}'", suffix));
				}
			}

			return number;
		}
	}
}