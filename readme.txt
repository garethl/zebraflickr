ZebraFlickr is a flickr uploader, and is in beta status. You use it at your own
risk!

ZebraFlickr is copyright (c) 2012 Gareth Lennox (garethl@gmail.com)

ZebraFlickr is open source software, see license.txt for licensing information.

----------

ZebraFlickr is a command line tool to easily bulk upload all or a portion
of your photos. It groups your photos into sets based on their folder grouping
and it automatically adds machine tags like the sha1 hash of each photo, and 
grouping.

Note that a zf-state.db file is created in the current directory, storing the
state of your uploaded images. It is not a good idea to lose this, as ZebraFlickr
doesn't yet have a sync mechanism to re-download from flickr.

Note that the first time you run ZebraFlickr, it will open a web page for you to
authorize it with flickr. Flickr will give you a code to paste into ZebraFlickr.

----------

Usage: zebraflickr <command> [<options>...]
Type 'zebraflickr help <command>' for help on a specific command.

Commands:
    skipped        Lists all files that have been skipped
	
    upload         Uploads all new and changed files to flickr

Global options:

  --nologo                   : Hide the product name and version number
  --verbosity=VALUE          : Verbosity level (Debug, Info, Warn, Error or
                               Fatal). Default = Info

----------

upload: Uploads all new and changed files to flickr
usage: upload    [options]

Options:
  --family                   : Upload photos with privacy set to family
  --friend                   : Upload photos with privacy set to friend
  --public                   : Upload photos with privacy set to public
  --levelignore=VALUE        : Ignores top level directories matching the
                               number given. Useful if you store your sets in
                               annual folders, for example.
  -p, --path=VALUE           : Root path of your photos
  -s, --subfolder=VALUE      : Subfolder to operate on, within the path

----------

skipped: Lists all files that have been skipped
usage: skipped   [options]
