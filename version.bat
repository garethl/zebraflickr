@echo off

SET version=0
FOR /F "tokens=1" %%a IN ('git rev-list master') DO SET /A version+=1
FOR /F "tokens=*" %%a IN ('git rev-parse HEAD') DO SET revision=%%a

echo %version%
echo %revision%